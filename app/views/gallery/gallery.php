<?php
use yii;
use yii\helpers\Url;
use vendor\noumo\easyii\models\Lang;
use yii\data\Pagination;
use yii\easyii\helpers\Image;

$asset = \app\assets\AppAsset::register($this);

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;

$this->title = Yii::t('easyii', 'Gallery');
?>

	<!--begin wrapper-->
	<div class="wrapper">
	
	<div class="content slide-galleria">
	
		<i>X</i>   
        <div id="galleria">
		
		<?php if(count($items)>0): ?>
		<?php foreach($items as $item): ?>

            <a href="#">
            	<img src="<?= $item->image ?>">
        	</a>
			
		<?php endforeach; ?>
		<?php endif; ?>
     	
        </div>
		
    </div>

		<!--breadcrumb-->
		<div class="breadcrumb">
			<div class="containers">
				<a href="/"><?= Yii::t('easyii', 'Main') ?></a> | <?= Yii::t('easyii', 'Gallery') ?>
			</div>
		</div>
		<div class="gallery-page">
			
			<div class="title-page"><?= Yii::t('easyii', 'Gallery') ?></div>
			<div class="gallery-block-line clearfix">
			
			<?php if(count($items)>0): ?>
			<?php foreach($items as $item): ?>
			
			<?php
			if($currLang == 'ru'){
				$description = $item->description;
			}
			else{
				$description = $item->{'description_'.$currLang};
			}
			if(!$description && $defLang != 'ru'){
				$description = $item->{'description_'.$defLang};
			}
			elseif(!$description){
				$description = $item->description;
			}

			?>
			
				<div class="gallery-block">
					<img src="<?= Image::thumb($item->image, 310, 148, true) ?>" alt="">
					<div class="gallery-info">
						<div class="gallery-date">
							<?= date('d.m.Y', $item->date) ?>
						</div>
						<div class="star" data-id="<?= $item->photo_id ?>" data-csrf="<?=Yii::$app->request->getCsrfToken()?>">
						<?php
						for($i=1; $i<=5; $i++){
						    if($i<=$item->rating){
							    echo '<div class="rating '.$i.' ratings" data-rating="'.$i.'"></div>';
							}
							else{
						        echo '<div class="rating '.$i.'" data-rating="'.$i.'"></div>';
							}
						}
						?>
						</div>
					</div>
					<?php if($description): ?>
					<p><?= $description ?></p>
					<?php endif; ?>
				</div>
				
			<?php endforeach; ?>
			<?php endif; ?>
				
			</div>
			
  		<?php 
		echo \yii\widgets\LinkPager::widget(['pagination' => $pagination, 'options' => ['class' => 'pagination clearfix']]); 
		?>

		</div>

		<!--text-page EnD-->

	</div>
	<!--end wrapper-->

<?php $this->registerJs("
    jQuery(function($){
	
	    //gallery
        // Load the classic theme
        Galleria.loadTheme('/media/js/galleria.classic.min.js');
        // Initialize Galleria
        $('#galleria').galleria();
		
		//voting	
		$('.rating').on('click', function(){
		    num = $(this).attr('data-rating');
			id = $(this).parent().attr('data-id');
			csrf = $(this).parent().attr('data-csrf');
			obj = $(this).parent();
		    $.ajax({
                url: '".Url::to(['@web/gallery/vote'], true)."',
                type: 'post',
				data: {_csrf: csrf, vote: num, id: id},
                success: function (data) {
					if(data){
					    ind = 1;
					    obj.find('.rating').each(function(i, obj){
					        if(ind<=data){
						       $(obj).addClass('ratings');
						    }
						    else{
						        $(obj).removeClass('ratings');
						    }
						    ind++;
					    });
					}
                }
            });
        });
		
    });");
?>