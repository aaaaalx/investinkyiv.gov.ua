<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\easyii\modules\article\models\Item;
use yii\easyii\modules\article\models\Category;
use yii\data\Pagination;
use vendor\noumo\easyii\models\Lang;

class EventsController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex($id=null)
    {
		$model = new Item;
		$cat = new Category;
		if($id){
		    $catModel = Category::find()->where('category_id = :id', [':id'=>$id])->one();
		    $cats = Category::find()->where('order_num = :order', [':order'=>$catModel->order_num])->all();
		}
		else{
		    $catModel = Category::find()->where('slug = "events"')->one();
			$cats = Category::find()->where('order_num = :order', [':order'=>$catModel->order_num])->all();
		}
		$cats_id = '';
		if(count($cats)>0){
		    foreach($cats as $val){
		        $cats_id .= ' OR category_id = '.$val->category_id;
		    }
		}
		$items = $model->find()->where('status = 1 AND category_id = :id'.$cats_id, ['id'=>$id])->sortDate();
		$pagination = new Pagination(['totalCount' => $items->count(), 'pageSize'=>4]);
		$items = $items->offset($pagination->offset)->limit($pagination->limit)->all();
        return $this->render('index', [
		    'model' => $model,
			'items' => $items,
			'pagination' => $pagination,
			'menu' => $catModel
		]);
    }
	
	public function actionView($slug)
    {
	    $model = new Item;
	    $pageModel = $model->find()->where('slug = :slug AND status = 1', [':slug' => $slug])->one();
		
		if($pageModel){
		
		    $pageModel->views += 1;
		    $pageModel->update();
		
	    	//seo
            $currLang = Lang::getCurrent()->url;
	    	if($currLang == 'ru'){
	    	    $h1 = $pageModel->getSeoText()->h1;
	    	    $title = $pageModel->getSeoText()->title;
	    	    $description = $pageModel->getSeoText()->description;
	    	    $keywords = $pageModel->getSeoText()->keywords;
	    	}
		    else{
		        $h1 = $pageModel->getSeoText()->{'h1_'.$currLang};
		        $title = $pageModel->getSeoText()->{'title_'.$currLang};
		        $description = $pageModel->getSeoText()->{'description_'.$currLang};
		        $keywords = $pageModel->getSeoText()->{'keywords_'.$currLang};
		    }		
		    \Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => $description,
            ]);
		    \Yii::$app->view->registerMetaTag([
                'name' => 'keywords',
                'content' => $keywords,
            ]);
		
		    return $this->render('view', [
		        'model' => $pageModel,
		    	'h1' => $h1,
		    	'title' => $title
		    ]);
		
		}
		else{
		    throw new \yii\web\NotFoundHttpException();
		}
    }

}