<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\easyii\widgets\WLang;
use yii\easyii\widgets\Wmenu;
use vendor\noumo\easyii\models\Lang;

$asset = \app\assets\AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="shortcut icon" href="<?= $asset->baseUrl ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?= $asset->baseUrl ?>/favicon.ico" type="image/x-icon">
        <?php $this->head() ?>
		
		<link rel="stylesheet" type="text/css" href="/media/css/bundle.min.css">
		<link rel="stylesheet" type="text/css" href="/media/css/reset.css">
	    <link rel="stylesheet" type="text/css" href="/media/css/tablet.css">	
    </head>
<body>
<?php $this->beginBody() ?>
<style media='print' type='text/css'>
	#navbar-iframe {display: none; height: 0px; visibility: hidden;}
	.noprint {display: none;}
	body {background:#FFF; color:#000;}
	a {text-decoration: underline; color:#00F;}
	}
</style>
<svg width="0" height="0">
  <clipPath id="clipPolygon">
    <polygon points="534 3,400 2,470 58">
    </polygon>
  </clipPath>
</svg>
<!--Begin Arrow-top-->


<!--end Arrow-top-->
	<!-- begin wrapper  -->
	<div class="wrapper">
		<!-- header -->
		<div class="header">
			<div class="containers">
				<a href="<?= Url::to(['/'], true) ?>" class="logo"> <p>Kyiv City State <br> Administration</p> <img src="<?= $asset->baseUrl ?>/img/logo-img.png" alt=""></a>
                <?php echo \yii\easyii\modules\text\api\Text::get('top-contacts'); ?>
				<a href="#" class="logo2">
					<img src="<?= $asset->baseUrl ?>/img/icon/k-icon.png" alt="">
					<p>Kyiv <br>Investment <br> Agency</p>
				</a>
                <?php echo \yii\easyii\modules\text\api\Text::get('top-links'); ?>
			</div>
		</div>
		<div class="header-bottom">
			<div class="containers mini-width">
			
				<form action="<?= Url::to(['/search'], true) ?>" method="get" class="search" name="search">
					<input type="text" name="search"> 
					<input type="submit" value="">
				</form>
				
				<?= WLang::widget(['view' => 'view2', 'asset' => $asset]);?>
				
				<?php echo \yii\easyii\modules\text\api\Text::get('top-mobile-contacts'); ?>
				
				<?= Wmenu::widget(['view' => 'main', 'menu' => 1, 'cssClass' => 'menu-head']) ?>
				<?= Wmenu::widget(['view' => 'main', 'menu' => 1, 'cssClass' => 'menu-head-popup', 'type' => 'pop-up']) ?>
				
			</div>

		</div>
		<!-- HEADER END -->
		
        <?= $content ?>
		
	</div>
	<!-- end wrapper -->

	<!-- begin footer  -->
	<div class="footer clearfix">
		<div class="containers">
			<div class="inform-block">
			<?php echo \yii\easyii\modules\text\api\Text::get('footer-contacts'); ?>
			</div>
			<div class="inform-block">
			<?php echo \yii\easyii\modules\text\api\Text::get('footer-support'); ?>
			</div>
			<form action="#" class="Subscription">				
				<b><?= Yii::t('easyii', 'Subscribe to news') ?></b>
				<input type="email" placeholder="<?= Yii::t('easyii', 'Email') ?>">
				<input type="submit" value="<?= Yii::t('easyii', 'Subscribe') ?>">
			</form>
			<i><?php echo \yii\easyii\modules\text\api\Text::get('footer-title'); ?></i>
			<i><?= Yii::t('easyii', 'Developer') ?>: kitsoft</i>

		</div>
	</div>
	<div class="block-links-one">
		<a href="#"><?= Yii::t('easyii', 'Is there ideas?') ?></a>
	</div>
	<div class="block-links-two">
		<a href="#"><?= Yii::t('easyii', 'Become an Investor') ?></a>
	</div>
	<div class="block-links-three">
		<a href="#"><?= Yii::t('easyii', 'Online help') ?></a>
	</div>
	<div class="block-links-four">
		<ul>
			<li><a href="#"><img src="<?= $asset->baseUrl ?>/img/twi-icon.png" alt=""></a></li>
			<li><a href="#"><img src="<?= $asset->baseUrl ?>/img/vk-icon.png" alt=""></a></li>
			<li><a href="#"><img src="<?= $asset->baseUrl ?>/img/tw-icon.png" alt=""></a></li>
			<li><a href="#"><img src="<?= $asset->baseUrl ?>/img/fb-icon.png" alt=""></a></li>
			<li><a href="#"><img src="<?= $asset->baseUrl ?>/img/in-icon.png" alt=""></a></li>
		</ul>
	</div>
	<div class="arrow-to-top">
		
	</div>
	<!-- end footer -->
	
    <?php $this->endBody() ?>
	
	<script src="/media/js/jquery.min.js"></script>
	<script src="/media/js/main.js"></script>

</body>
</html>
<?php $this->endPage() ?>