<?php
namespace yii\easyii\widgets;

class Wdocuments extends \yii\bootstrap\Widget
{

    public $view;

    public function init(){
	    
	}

    public function run() {
	    $view = 'documents/'.($this->view?$this->view:'view');
	    return $this->render($view);
    }
}