<?php
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use vendor\noumo\easyii\models\Lang;

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;

?>

				<div class="download-documents-box">
				<a href="<?= Url::to(['/documents/']) ?>" class="download-button"><?= Yii::t('easyii', 'Documents') ?></a>

				<!--begin ul documents-->
				<ul class="down-doc">
				
				<?php
			    $docsList = \yii\easyii\modules\article\models\Item::find()->where('status = 1 AND category_id = 4 AND document IS NOT NULL')->limit(4)->sortDate()->all();
			    ?>
			
			    <?php if(count($docsList)>0): ?>		
			    <?php foreach($docsList as $doc): ?>
			
			    <?php
			    if($currLang == 'ru'){
			        $docTitle = $doc->title;
			    }
			    else{
			        $docTitle = $doc->{'title_'.$currLang};
			    }
			    if(!$docTitle && $defLang != 'ru'){
				    $docTitle = $doc->{'title_'.$defLang};
			    }
			    elseif(!$docTitle){
			        $docTitle = $doc->title;
			    }
				
			    ?>
			
			    <?php if($docTitle): ?>
				
					<li><a href="<?= Url::to(['site/download?doc='.base64_encode($doc->document)]) ?>"><?= $docTitle ?></a></li>
					
				<?php endif; ?>
			
			    <?php endforeach; ?>
			    <?php endif; ?>
				
				</ul>
				<!--end ul documents-->

				<div class="all-documents-box">
				
					<a href="<?= Url::to(['/documents/']) ?>" class="blue-button all-documents"><?= Yii::t('easyii', 'All documents') ?></a>
				
				</div>

			</div>