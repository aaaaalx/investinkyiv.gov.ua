<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;
use yii\easyii\modules\catalog\api\Catalog;
use vendor\noumo\easyii\models\Lang;
use yii\bootstrap\Tabs;
use yii\bootstrap\Collapse;
use yii\easyii\modules\page\models\Page;

$cats = Catalog::cats();
$catsA = [];
if(count($cats)>0){
    foreach($cats as $val){
	    $catsA[$val->category_id] = $val->title;
	}
}
?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['class' => 'model-form']
]); ?>

<?php

$itemsList = [];
$itemsList[] = [
	'label' => 'Українська',
	'content' => langField($form, $model, 'ua', Lang::getDefaultLang()->url),
	'active' => true
];
$itemsList[] = [
	'label' => 'Русский',
	'content' => langField($form, $model, 'ru', Lang::getDefaultLang()->url),
	'active' => false
];
$itemsList[] = [
	'label' => 'English',
	'content' => langField($form, $model, 'en', Lang::getDefaultLang()->url),
	'active' => false
];

?>

<?php echo Tabs::widget([
    'items' => $itemsList
]);
?>

<?php
function langField($form, $model, $lang, $defLang){
    if($lang == 'ru'){
        $title = 'title';
	    $text = 'text';
		$code = 'code';
	}
	else{
        $title = 'title_'.$lang;
	    $text = 'text_'.$lang;
		$code = 'code_'.$lang;
	}
	$res = '';
	$res .= $form->field($model, $title);
	$res .= $form->field($model, $text)->widget(Redactor::className(),[
        'options' => [
            'minHeight' => 400,
            'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
            'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'pages']),
            'plugins' => ['fullscreen']
        ]
    ]);
	if(IS_ROOT){
	    $res .= SeoForm::widget(['model' => $model, 'lang' => $lang]);
	}

    $res .= Collapse::widget([
        'items' => [
            [
                'label' => Yii::t('easyii','Insert code'),
                'content' => $form->field($model, $code)->textarea(),
                'contentOptions' => [
                    //'class' => 'in'
                ]
            ]
        ]
    ]);
	
	return $res;
}
?>

<?php echo Tabs::widget([
    'items' => [
	    [
	        'label' => Yii::t('easyii','General options for all languages'),
		    'active' => true
		]
	],
]);
?>

<?php if(IS_ROOT) : ?>
    <?= $form->field($model, 'slug') ?>
<?php endif; ?>

<?= $form->field($model, 'category_id')->dropDownList($catsA, ['prompt'=>Yii::t('easyii/page','-Choose a category-')])->label(Yii::t('easyii/page','Category')) ?>

<?= $form->field($model, 'status')->dropDownList($model->getStatusList())->label(Yii::t('easyii/page','Status')) ?>

<?= $form->field($model, 'template')->dropDownList(\vendor\noumo\easyii\models\Templates::getTemplatesList('page')) ?>

<?= Html::submitButton(Yii::t('easyii','Save'), ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>

<?php $this->registerJs("
    jQuery(function($){
        $('#w1 li').on('click', function(){
		    $('#subButt').attr('value', $(this).find('a').html());
			ind = $(this).find('a').attr('href');
			$('.findAttrTitle').attr('id', '');
			$(ind).find('.findAttrTitle').attr('id', 'page-title');
        });
    });");
?>