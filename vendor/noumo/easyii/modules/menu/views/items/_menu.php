<?php
use yii\helpers\Url;

$action = $this->context->action->id;
$module = $this->context->module->id;

?>
<?php if(IS_ROOT) : ?>
<ul class="nav nav-pills">

    <?php if($action == 'index') : ?>
        <li><a href="<?= Url::to(['/admin/'.$module]) ?>"><i class="glyphicon glyphicon-chevron-left font-12"></i> <?= Yii::t('easyii/menu', 'Menu') ?></a></li>
    <?php endif; ?>

    <li <?= ($action === 'index') ? 'class="active"' : '' ?>>
        <a href="<?= $this->context->getReturnUrl(['/admin/'.$module]) ?>">
            <?php if($action === 'edit') : ?>
                <i class="glyphicon glyphicon-chevron-left font-12"></i>
            <?php endif; ?>
            <?= Yii::t('easyii', 'List') ?>
        </a>
    </li>
    <li <?= ($action === 'create') ? 'class="active"' : '' ?>>
	    <a href="<?= Url::to(['/admin/'.$module.'/items/create/'.$parent]) ?>">
	    <?= Yii::t('easyii', 'Create') ?>
	    </a>
	</li>
</ul>
<br/>
<?php elseif($action === 'edit') : ?>
    <ul class="nav nav-pills">
        <li>
            <a href="<?= $this->context->getReturnUrl(['/admin/menu'])?>">
                <i class="glyphicon glyphicon-chevron-left font-12"></i>
                <?= Yii::t('easyii/menu', 'Menu') ?>
            </a>
        </li>
    </ul>
    <br/>
<?php endif; ?>
