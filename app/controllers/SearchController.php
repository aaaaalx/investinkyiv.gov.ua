<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\easyii\modules\article\models\Item;
use yii\easyii\modules\news\models\News;
use yii\easyii\modules\page\models\Page;
use yii\data\Pagination;

use yii\db\Query;

class SearchController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
	    $search = Yii::$app->request->get('search'); 
		$pages = null;
		$items = null;
	    if($search){
            $query1 = new Query();
            $query1->select("title, title_en, title_ua, slug, time, template")->from('easyii_news')->where('status = 1')
            ->andWhere(['like', 'title', $search])
            ->orWhere(['like', 'title_en', $search])
            ->orWhere(['like', 'title_ua', $search]);
	   
            $query2 = new Query();
            $query2->select('title, title_en, title_ua, slug, time, template')->from('easyii_article_items')->where('status = 1')
            ->andWhere(['like', 'title', $search])
            ->orWhere(['like', 'title_en', $search])
            ->orWhere(['like', 'title_ua', $search]);
			
			$query3 = new Query();
            $query3->select('title, title_en, title_ua, slug, time, template')->from('easyii_pages')->where('status = 1')
            ->andWhere(['like', 'title', $search])
            ->orWhere(['like', 'title_en', $search])
            ->orWhere(['like', 'title_ua', $search]);

            $query1->union($query2, true);
			$query1->union($query3, true);
	   
            $query = new Query();
            $query->select('*')->from(['u' => $query1]);
	   
            $pagination = new Pagination(['totalCount' => $query->count(), 'pageSize'=>10]);
            $items = $query->offset($pagination->offset)->limit(10)->all();
		}
		
		return $this->render('index', [
			'items' => $items,
			'pagination' => $pagination,
		]);

    }

}