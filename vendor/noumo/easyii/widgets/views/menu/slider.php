<?php
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use vendor\noumo\easyii\models\Lang;

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;

$asset = \app\assets\AppAsset::register($this);
?>
<?php if(count($items)>0): ?>
<ul>
    <?php foreach($items as $val): ?>
	<?php
	$settings = json_decode($val->settings);
	
	if($currLang == 'ru'){
		$name = $val->name;
	}
	else{
		$name = $val->{'name_'.$currLang};
	}
	if(!$name && $defLang != 'ru'){
		$name = $val->{'name_'.$defLang};
	}
	elseif(!$name){
		$name = $val->name;
	}
	
	?>
	<li class="<?= $settings->class?$settings->class:'' ?>"><a href="<?= $menu->checkHttp($val->link) ?>" title="<?= $name ?>"><?= $name ?><img src="<?= $asset->baseUrl ?>/img/other/arrow-menu.png" alt=""></a></li>
	<?php endforeach; ?>		
	
</ul>
<?php endif; ?>