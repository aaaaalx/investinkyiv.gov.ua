<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['class' => 'model-form']
]); ?>

<?php echo Tabs::widget([
    'items' => [
	    [
	        'label' => Yii::t('easyii','General options for all languages'),
		    'active' => true
		]
	],
]);
?>

<?= $form->field($model, 'title') ?>
<?= (IS_ROOT) ? $form->field($model, 'slug') : '' ?>

<?php echo Tabs::widget([
    'items' => [
	    [
	        'label' => 'Українська',
		    'active' => true
		]
	],
]);
?>

<?= $form->field($model, 'text_ua')->textarea() ?>

<?php echo Tabs::widget([
    'items' => [
	    [
	        'label' => 'Русский',
		    'active' => true
		]
	],
]);
?>

<?= $form->field($model, 'text')->textarea() ?>

<?php echo Tabs::widget([
    'items' => [
	    [
	        'label' => 'English',
		    'active' => true
		]
	],
]);
?>

<?= $form->field($model, 'text_en')->textarea() ?>

<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>