<?php
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use vendor\noumo\easyii\models\Lang;
use vendor\noumo\easyii\modules\menu\models\MenuItems;

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;

?>

<?php if(!$type): ?>

<ul class="<?= $cssClass ?>">
    <?php if(count($items)>0): ?>
    <?php foreach($items as $val): ?>
	
	<?php
	if($currLang == 'ru'){
		$name = $val->name;
	}
	else{
		$name = $val->{'name_'.$currLang};
	}
	if(!$name && $defLang != 'ru'){
		$name = $val->{'name_'.$defLang};
	}
	elseif(!$name){
		$name = $val->name;
	}

	?>
	
	<?php if($name): ?>
	<li><a href="<?= $menu->checkHttp($val->link) ?>" title="<?= $name ?>"><?= $name ?></a>
	<?php
	    
		$menuItemsObj = MenuItems::find()->where('parent = :parent AND status = 1', [':parent' => $val->id])->all();
		if($menuItemsObj){
			echo MenuItems::getChildren($menuItemsObj, $menu);
		}
	?>
	</li>
	<?php endif; ?>
	<?php endforeach; ?>		
	<?php endif; ?>
</ul>

<?php elseif($type == 'pop-up'): ?>

<ul class="<?= $cssClass ?>">
    <li class="first-head-pop"><?= Yii::t('easyii', 'Menu') ?></li>
    <?php if(count($items)>0): ?>
    <?php foreach($items as $val): ?>
	
	<?php
	if($currLang == 'ru'){
		$name = $val->name;
	}
	else{
		$name = $val->{'name_'.$currLang};
	}
	if(!$name && $defLang != 'ru'){
		$name = $val->{'name_'.$defLang};
	}
	elseif(!$name){
		$name = $val->name;
	}
	?>
	<?php if($name): ?>
	<li><a href="<?= $menu->checkHttp($val->link) ?>" title="<?= $name ?>"><?= $name ?></a></li>
	<?php endif; ?>
	<?php endforeach; ?>	
	<?php endif; ?>
</ul>

<?php elseif($type == 'right-menu'): ?>

<div class="right-menu">
	<ul>
	<?php if(count($items)>0): ?>
    <?php foreach($items as $val): ?>
	
	<?php
	if($currLang == 'ru'){
		$name = $val->name;
	}
	else{
		$name = $val->{'name_'.$currLang};
	}
	if(!$name && $defLang != 'ru'){
		$name = $val->{'name_'.$defLang};
	}
	elseif(!$name){
		$name = $val->name;
	}
	?>
	<?php if($name): ?>
		<li><a href="<?= $menu->checkHttp($val->link) ?>"><?= $name ?></a></li>
	<?php endif; ?>
	<?php endforeach; ?>	
	<?php endif; ?>
	</ul>
</div>

<?php endif; ?>