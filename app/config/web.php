<?php

$params = require(__DIR__ . '/params.php');

$basePath =  dirname(__DIR__);
$webroot = dirname($basePath);

$config = [
    'id' => 'app',
    'basePath' => $basePath,
    'bootstrap' => ['log'],
    'language' => 'en-US',
    'runtimePath' => $webroot . '/runtime',
    'vendorPath' => $webroot . '/vendor',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'GhFVNaTDHLhdalOFMyik',
			'class' => 'yii\easyii\components\LangRequest',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
        ],
        'urlManager' => [
		    'class'=>'yii\easyii\components\LangUrlManager',
            'rules' => [
			
			
				
				'admin' => 'admin/default/index',
				'admin/<controller:\w+>/<action:[\w-]+>' => 'admin/<controller>/<action>',
				'admin/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => 'admin/<controller>/<action>',
				
				'category/<category:[\w_\/-]+>/<slug:[\w-]+>' => 'category/article',
				'category/<category:[\w_\/-]+>' => 'category/category',
				
				'gallery/vote' => 'gallery/vote',				
				'gallery/<id:\d+>' => 'gallery/index',
				
				'search' => 'search/index',
				
			    'events/<id:\d+>' => 'events/index',
				'events' => 'events/index',
			    'events/<slug:[\w-]+>' => 'events/view',
				
				'news/' => 'news/index',
				'news/<slug:[\w-]+>' => 'news/view',
				
				'<slug:[\w-]+>' => 'site/page',
			
			    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',			
                '<controller:\w+>/view/<slug:[\w-]+>' => '<controller>/view',			
				'<controller:\w+>/<action:\w+>/<slug:[\w-]+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/cat/<slug:[\w-]+>' => '<controller>/cat',

            ],
        ],
        'assetManager' => [
            // uncomment the following line if you want to auto update your assets (unix hosting only)
            //'linkAssets' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [YII_DEBUG ? 'jquery.js' : 'jquery.min.js'],
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [YII_DEBUG ? 'css/bootstrap.css' : 'css/bootstrap.min.css'],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [YII_DEBUG ? 'js/bootstrap.js' : 'js/bootstrap.min.js'],
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
    
    $config['components']['db']['enableSchemaCache'] = false;
}

return array_merge_recursive($config, require($webroot . '/vendor/noumo/easyii/config/easyii.php'));