<?php
use yii\helpers\Url;
use vendor\noumo\easyii\models\Lang;
use yii\easyii\helpers\Image;

use yii\easyii\modules\news\api\Article;

$asset = \app\assets\AppAsset::register($this);

$module = 'events';

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;

if($currLang == 'ru'){
	$eventsTitle = $model->title;
	$eventsText = $model->text;
}
else{
	$eventsTitle = $model->{'title_'.$currLang};
	$eventsText = $model->{'text_'.$currLang};
}
if(!$eventsTitle && $defLang != 'ru'){
	$eventsTitle = $model->{'title_'.$defLang};
	$eventsText = $model->{'text_'.$defLang};
}
elseif(!$eventsTitle){
	$eventsTitle = $item->title;
	$eventsText = $item->text;
}

$this->title = $title?$title:$eventsTitle;

?>
	<!--begin wrapper-->
	<div class="wrapper">

		<!--breadcrumb-->
		<div class="breadcrumb">
			<div class="containers">
				<a href="<?= Url::to(['/']) ?>"><?= Yii::t('easyii', 'Main') ?></a> | <a href="<?= Url::to(['/'.$module.'/']) ?>"><?= Yii::t('easyii', 'Events') ?></a> | <?= $eventsTitle ?>
			</div>
		</div>
		<div class="text-page">

			<div class="left-column">
				<div class="last-docs-date"><p class="p-last-docs"><?= date('d.m.Y, H:i', $model->time) ?></p></div>
				
				<b><?= $eventsTitle ?></b>
				<?php if($model->image): ?>
				<img src="<?= Image::thumb($model->image, 710) ?>" alt="<?= $eventsTitle ?>">
				<?php endif; ?>
				
				<?= $eventsText ?>
				
				<div class="print-link">
					<a href="javascript:window.print(); void 0;"><?= Yii::t('easyii', 'Print') ?></a>
				</div>
			</div>
			<div class="right-column">

				<a href="<?= Url::to(['/news/']) ?>" class="download-button"><?= Yii::t('easyii', 'Popular among users') ?></a>
				
			<?php
			$newsItems = \yii\easyii\modules\news\models\News::find()->where('status = 1')->orderBy('views')->limit(6)->all();
			?>
			<?php if(count($newsItems)>0): ?>
			<?php foreach($newsItems as $item): ?>
			
			<?php
			if($currLang == 'ru'){
			    $newsTitle = $item->title;
			}
			else{
			    $newsTitle = $item->{'title_'.$currLang};
			}
			if(!$newsTitle && $defLang != 'ru'){
				$newsTitle = $item->{'title_'.$defLang};
			}
			elseif(!$newsTitle){
			    $newsTitle = $item->title;
			}
			
			mb_internal_encoding("UTF-8");
			$month = Yii::$app->formatter->asDate($item->time, 'php:F');
			?>
			<?php if($newsTitle): ?>
				<div class="events-content clearfix">
					<div class="events-date">
						<p class="p-day"><?= date('d', $item->time) ?></p>
						<p class="p-month"><?= mb_substr($month, 0, 3) ?></p>
					</div>
					<a href="<?= Url::to(['/news/'.$item->slug]) ?>"><p class="p-events-content"><?= $newsTitle ?></p>
					<img src="<?= $asset->baseUrl ?>/img/arrow.png" class="arrow"></a>
				</div>
			<?php endif; ?>
			<?php endforeach; ?>
			<?php endif; ?>

				<div class="all-documents-box">
					<a href="<?= Url::to(['/news/']) ?>" class="blue-button all-documents"><?= Yii::t('easyii', 'All news') ?></a>
				</div>
				<!--end all-documents-button-->

			</div>

		</div>
		<!--text-page EnD-->

	</div>
	<!--end wrapper-->
