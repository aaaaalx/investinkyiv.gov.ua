<?php
return [
    'Files' => 'Файли',
    'Create file' => 'Створити файл',
    'Edit file' => 'Редагувати файл',
    'File created' => 'Файл створено успішно',
    'File updated' => 'Файл оновлено',
    'File error. {0}' => 'Помилка файлу. {0}',
    'File deleted' => 'Файл видалено',

    'Size' => 'Розмір',
    'Downloads' => 'Завантажень',
    'Download file' => 'Завантажити файл',
];