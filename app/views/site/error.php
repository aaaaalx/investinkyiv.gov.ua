<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
?>

	<!--begin wrapper-->
	<div class="wrapper">
    
	<?php if($this->title == 'Not Found (#404)'): ?>
	<!--ERROR-->
		<div class="error-page">
			<div class="h2-error"><?= Yii::t('easyii', 'Page not found!') ?></div>
			<p>
				<b><?= Yii::t('easyii', 'Error 404.') ?></b><br>
				<?= Yii::t('easyii', 'The page you requested was not found. You may have entered the wrong url address,') ?><br> 
				<?= Yii::t('easyii', 'or such page no longer exists.') ?></p>
			<div class="all-news-button-box"><a href="<?= Url::to(['/']) ?>" class="blue-button all-news"><?= Yii::t('easyii', 'Home') ?></a></div>
		</div>
	<!--END ERROR-->
	<?php else: ?>
	<!--ERROR-->
		<div class="error-page">
			<div class="h2-error"><?= Yii::t('easyii', 'There was an error!') ?></div>
			<p><?= Yii::t('easyii', 'Something wrong. Check, please, the url address or return') ?> <br> 
			<?= Yii::t('easyii', 'To the main page.') ?></p>
			<div class="all-news-button-box"><a href="<?= Url::to(['/']) ?>" class="blue-button all-news"><?= Yii::t('easyii', 'Home') ?></a></div>
		</div>

	<!--END ERROR-->
	<?php endif; ?>
	</div>
	<!--end wrapper-->