<?php
return [
    'Gallery' => 'Фотогалерея',
	'Photo Gallery' => 'Фотогалерея',
    'Albums' => 'Альбоми',
    'Create album' => 'Створити альбом',
    'Edit album' => 'Редагувати альбом',
    'Album created' => 'Альбом створено успішно',
    'Album updated' => 'Альбом оновлено',
    'Album deleted' => 'Альбом видалено',
];