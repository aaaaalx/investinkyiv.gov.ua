<?php
namespace yii\easyii\widgets;

use yii\base\InvalidConfigException;

class Wblock extends \yii\bootstrap\Widget
{

    public $model;
	public $view;
	public $category;

    public function init(){
	
	    if (empty($this->model) && empty($this->view)) {
            throw new InvalidConfigException('Required param isn\'t set.');
        }
	
	}

    public function run() {    
			return $this->render('block/main', [
                'lang' => $this->lang,
			    'model' => $this->model,
				'category' => $this->category,
            ]);
		}
    }
}