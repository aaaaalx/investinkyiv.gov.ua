<?php
use yii\helpers\Url;
use vendor\noumo\easyii\models\Lang;

$asset = \app\assets\AppAsset::register($this);

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;

if($currLang == 'ru'){
	$pageTitle = $model->title;
	$pageText = $model->text;
	$pageCode = $model->code;
}
else{
	$pageTitle = $model->{'title_'.$currLang};
	$pageText = $model->{'text_'.$currLang};
	$pageCode = $model->{'code_'.$currLang};
}
if(!$pageTitle && $defLang != 'ru'){
	$pageTitle = $model->{'title_'.$defLang};
	$pageText = $model->{'text_'.$defLang};
	$pageCode = $model->{'code_'.$defLang};
}
elseif(!$pageTitle){
	$pageTitle = $model->title;
	$pageText = $model->text;
	$pageCode = $model->code;
}

$this->title = $title?$title:$pageTitle;

?>

                <?= $pageText ?>
			    <?= $pageCode ?>
