<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\easyii\modules\news\models\News;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use vendor\noumo\easyii\models\Lang;

class NewsController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
		$model = new News;
		$items = $model->find()->where('status = 1')->sortDate();
		$pagination = new Pagination(['totalCount' => $items->count(), 'pageSize'=>4]);
		$items = $items->offset($pagination->offset)->limit($pagination->limit)->all();
        return $this->render('index', [
		    'model' => $model,
			'items' => $items,
			'pagination' => $pagination,
		]);
    }
	
	public function actionView($slug)
    {
	    $model = new News;
	    $pageModel = $model->find()->where('slug = :slug AND status = 1', [':slug' => $slug])->one();
		
		if($pageModel){
		
		    $pageModel->views += 1;
		    $pageModel->update();
			
		    //seo
            $currLang = Lang::getCurrent()->url;
		    if($currLang == 'ru'){
		        $h1 = $pageModel->getSeoText()->h1;
		        $title = $pageModel->getSeoText()->title;
		        $description = $pageModel->getSeoText()->description;
		        $keywords = $pageModel->getSeoText()->keywords;
		    }
		    else{
		        $h1 = $pageModel->getSeoText()->{'h1_'.$currLang};
		        $title = $pageModel->getSeoText()->{'title_'.$currLang};
		        $description = $pageModel->getSeoText()->{'description_'.$currLang};
		        $keywords = $pageModel->getSeoText()->{'keywords_'.$currLang};
		    }		
		    \Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => $description,
            ]);
		    \Yii::$app->view->registerMetaTag([
                'name' => 'keywords',
                'content' => $keywords,
            ]);
		
		    return $this->render('view', [
		        'model' => $pageModel,
		    	'h1' => $h1,
		    	'title' => $title
		    ]);
		
		}
		else{
		    throw new \yii\web\NotFoundHttpException();
		}
    }
}