<?php
namespace app\assets;

class AppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        //'css/bundle.min.css',
		//'css/form.css',
		//'css/reset.css',
		//'css/tablet.css'
    ];
    public $js = [
		//'js/main.js',
		//'js/jquery.min.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
