<?php
namespace vendor\noumo\easyii\modules\menu\models;

use Yii;
use vendor\noumo\easyii\models\Lang;

/**
 * This is the model class for table "easyii_menu_items".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_en
 * @property string $name_ua
 * @property integer $parent
 * @property integer $menu
 * @property string $type
 * @property integer $link_id
 * @property string $link
 * @property string $settings
 * @property integer $status
 * @property integer $depth
 * @property integer $position
 */
class MenuItems extends \yii\easyii\components\ActiveRecord
{
    const TYPE_PAGE = 'page';
	const TYPE_NEWS = 'news';
	const TYPE_ARTICLE = 'article';
	const TYPE_CATEGORY_ARTICLE = 'category_article';
	const TYPE_CATEGORY = 'category';
	
	const STATUS_ON = 1;
	const STATUS_OFF = 0;
	
	//get an array of status list
	public static function getStatusList()
    {
        return [
		    self::STATUS_ON => Yii::t('easyii/menu', 'Published'),
			self::STATUS_OFF => Yii::t('easyii/menu', 'Unpublished'),		
		];
    }
	
	//get an array of children by parent id
    public static function getChildren($parent, $menu)
    {
	    $defLang = Lang::getDefaultLang()->url;
        $currLang = Lang::getCurrent()->url;
		
	    $str = '';
		$str .= '<ul>';
		foreach($parent as $val){
		
			if($currLang == 'ru'){
		        $name = $val->name;
	        }
	        else{
		        $name = $val->{'name_'.$currLang};
	        }
	        if(!$name && $defLang != 'ru'){
		        $name = $val->{'name_'.$defLang};
	        }
	        elseif(!$name){
		        $name = $val->name;
	        }
			
		    if($name){
		        $str .= '<li><a href="'.$menu->checkHttp($val->link).'" title="'.$name.'">'.$name.'</a></li>';
				$menuItemsObj = MenuItems::find()->where('parent = :parent AND status = 1', [':parent' => $val->id])->all();
				if($menuItemsObj){
				    $str .= MenuItems::getChildren($menuItemsObj);
				}
				$str .= '</li>';
			}	
		}
		$str .= '</ul>';
		
        return $str;
    }
	
	//get an array of root menu items
	public static function getParent($menu, $current = null){
	    if(isset($current)){
            $items = self::find()->where('menu = :menu AND id != :current', [':menu' => $menu, ':current' => $current])->all();
		}
	    else{
		    $items = self::find()->where('menu = :menu', [':menu' => $menu])->all();
		}
        $itemsA = [];
        if(count($items)>0){
            foreach($items as $item){
			
			    $defLang = \vendor\noumo\easyii\models\Lang::getDefaultLang()->url;
			    $currLang = \vendor\noumo\easyii\models\Lang::getCurrent()->url;

			    if($currLang == 'ru'){
				    $title = $item['name'];
			    }
			    else{
			    	$title = $item['name_'.$currLang];
			    }
			    if(!$title && $defLang != 'ru'){
			    	$title = $item['name_'.$defLang];
			    }
			    elseif(!$title){
			    	$title = $item['name'];
			    }
		
	            $itemsA[$item->id] = $title;
	        }
        }
		return $itemsA;
	}
	
    //get an array of content types
    public static function getTypesList()
    {
        return [
		    self::TYPE_PAGE => Yii::t('easyii/menu', 'Pages'),
			self::TYPE_NEWS => Yii::t('easyii/menu', 'News'),
			self::TYPE_ARTICLE => Yii::t('easyii/menu', 'Articles'),
			self::TYPE_CATEGORY_ARTICLE => Yii::t('easyii/menu', 'Categories of articles'),
			self::TYPE_CATEGORY => Yii::t('easyii/menu', 'Categories'),
		];
    }
	
	//get flat array of menu items
	public static function flatArray($enterA, $resultA, $start){
        foreach($enterA as $key => $val){
	        foreach($val as $key2 => $val2){
		       $resultA[$start][$key2] = $val2;
	        }
			$children = self::find()->where('parent = :id', [':id' => $resultA[$start]['id']])->orderBy('position')->asArray()->all();
		    if(count($children)>0){	    
			    $resultA[$start]['children'] = $children;
				$start++;
			    $resultA = self::flatArray($children, $resultA, $start);
			    $start = $resultA[0];
				$resultA = $resultA[1];
		    }
			else{
			    $start++;
			}
        }
		return [$start, $resultA];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'easyii_menu_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ua', 'menu', 'status'], 'required'],
            [['parent', 'menu', 'link_id', 'status', 'depth', 'position'], 'integer'],
            [['name', 'name_en', 'name_ua', 'type', 'link', 'settings'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('easyii/menu', 'ID'),
            'name' => Yii::t('easyii/menu', 'Name'),
			'name_en' => Yii::t('easyii/menu', 'Name'),
			'name_ua' => Yii::t('easyii/menu', 'Name'),
            'parent' => Yii::t('easyii/menu', 'Parent'),
            'menu' => Yii::t('easyii/menu', 'Menu'),
            'type' => Yii::t('easyii/menu', 'Type'),
            'link_id' => Yii::t('easyii/menu', 'Link ID'),
            'link' => Yii::t('easyii/menu', 'Link'),
            'settings' => Yii::t('easyii', 'Settings'),
            'status' => Yii::t('easyii/menu', 'Status'),
			'depth' => Yii::t('easyii/menu', 'Depth'),
			'position' => Yii::t('easyii/menu', 'Position'),
        ];
    }

    /**
     * @inheritdoc
     * @return MenuItemsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MenuItemsQuery(get_called_class());
    }
}
