<?php
use yii\helpers\Url;
use vendor\noumo\easyii\modules\menu\models\MenuItems;
use vendor\noumo\easyii\models\Lang;

\yii\bootstrap\BootstrapPluginAsset::register($this);

$this->title = Yii::$app->getModule('admin')->activeModules[$this->context->module->id]->title;

$baseUrl = '/admin/menu';

$newItems = [];
$children = false;
if(count($items)>0){
    $newItems = MenuItems::flatArray($items, [], 0);
    $newItems = $newItems[1];
}

?>

<?= $this->render('_menu', ['parent' => $id]) ?>

<?php if(sizeof($newItems) > 0) : ?>
    <table class="table table-hover">
        <tbody>
            <?php foreach($newItems as $item) : ?>
					
			<?php
			$defLang = Lang::getDefaultLang()->url;
			$currLang = Lang::getCurrent()->url;

			if($currLang == 'ru'){
				$title = $item['name'];
			}
			else{
				$title = $item['name_'.$currLang];
			}
			if(!$title && $defLang != 'ru'){
				$title = $item['name_'.$defLang];
			}
			elseif(!$title){
				$title = $item['name'];
			}
			?>
			
			
                <tr>
                    <td width="50"><?= $item['id'] ?></td>
                    <td style="padding-left:  <?= $item['depth'] * 20 ?>px;">
                        <?php if(count($item['children'])) : ?>
                            <i class="caret"></i>
                        <?php endif; ?>

                        <a href="<?= Url::to([$baseUrl.'/items/edit', 'id' => $item['id']]) ?>" <?= ($item['status'] == MenuItems::STATUS_OFF ? 'class="smooth"' : '') ?>><?= $title ?></a>

                    </td>
                    <td width="120" class="text-right">
                        <div class="dropdown actions">
                            <i id="dropdownMenu<?= $item['id'] ?>" data-toggle="dropdown" aria-expanded="true" title="<?= Yii::t('easyii', 'Actions') ?>" class="glyphicon glyphicon-menu-hamburger"></i>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu<?= $item['id'] ?>">
                                <li><a href="<?= Url::to([$baseUrl.'/items/edit', 'id' => $item['id']]) ?>"><i class="glyphicon glyphicon-pencil font-12"></i> <?= Yii::t('easyii', 'Edit') ?></a></li>
                                <li role="presentation" class="divider"></li>
								
                                <!--<li><a href="<?= Url::to([$baseUrl.'/items/up', 'id' => $item['id']]) ?>"><i class="glyphicon glyphicon-arrow-up font-12"></i> <?= Yii::t('easyii', 'Move up') ?></a></li>-->
                                <!--<li><a href="<?= Url::to([$baseUrl.'/items/down', 'id' => $item['id']]) ?>"><i class="glyphicon glyphicon-arrow-down font-12"></i> <?= Yii::t('easyii', 'Move down') ?></a></li>-->
                                
								<li role="presentation" class="divider"></li>
                                <?php if($item['status'] == MenuItems::STATUS_ON) :?>
                                    <li><a href="<?= Url::to([$baseUrl.'/items/off', 'id' => $item['id']]) ?>" title="<?= Yii::t('easyii', 'Turn Off') ?>'"><i class="glyphicon glyphicon-eye-close font-12"></i> <?= Yii::t('easyii', 'Turn Off') ?></a></li>
                                <?php else : ?>
                                    <li><a href="<?= Url::to([$baseUrl.'/items/on', 'id' => $item['id']]) ?>" title="<?= Yii::t('easyii', 'Turn On') ?>"><i class="glyphicon glyphicon-eye-open font-12"></i> <?= Yii::t('easyii', 'Turn On') ?></a></li>
                                <?php endif; ?>
                                <li><a href="<?= Url::to([$baseUrl.'/items/delete', 'id' => $item['id']]) ?>" class="confirm-delete" data-reload="1" title="<?= Yii::t('easyii', 'Delete item') ?>"><i class="glyphicon glyphicon-remove font-12"></i> <?= Yii::t('easyii', 'Delete') ?></a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
<?php else : ?>
    <p><?= Yii::t('easyii', 'No records found') ?></p>
<?php endif; ?>