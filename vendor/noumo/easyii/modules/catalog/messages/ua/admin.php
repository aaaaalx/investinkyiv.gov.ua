<?php
return [
    'Catalog' => 'Каталог',
    'Category fields' => 'Поля категорії',
    'Manage fields' => 'Редагування поля',

    'Add field' => 'Додати поле',
    'Save fields' => 'Сберегти поля',
    'Type options with `comma` as delimiter' => 'Елементи списку через кому',
    'Fields' => 'Поля',

    'Item created' => 'Новий запис створено успішно',
    'Item updated' => 'Запис оновлено',
    'Item deleted' => 'Запис видалено',

    'Title' => 'Назва',
    'Type' => 'Тип поля',
    'Options' => 'Опції',
    'Available' => 'Доступно',
    'Price' => 'Ціна',
    'Discount' => 'Знижка',
    'Select' => 'Обрати',
];