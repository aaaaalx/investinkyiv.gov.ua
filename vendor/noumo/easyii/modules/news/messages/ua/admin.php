<?php
return [
    'News' => 'Новини',
    'Create news' => 'Створити новину',
    'Edit news' => 'Редагувати новину',
    'News created' => 'Новину успішно створено',
    'News updated' => 'Новину оновлено',
    'News deleted' => 'Новину видалено',
    'Short' => 'Коротко',
];