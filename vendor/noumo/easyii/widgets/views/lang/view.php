<?php
use yii\helpers\Html;
?>
<div class="lang" id="lang">
    <span class="current-lang" id="current-lang"><?= $current->name;?><span class="show-more-lang">▼</span></span>
    <ul class="langs" id="langs">
        <?php foreach ($langs as $lang):?>
            <li class="item-lang">
                <?= Html::a($lang->name, '/'.$lang->url.Yii::$app->getRequest()->getLangUrl()) ?>
            </li>
        <?php endforeach;?>
    </ul>
</div>