<?php
use yii\helpers\Url;
use vendor\noumo\easyii\models\Lang;
use yii\easyii\helpers\Image;

use yii\easyii\modules\news\api\News;

$module = 'news';

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;

if($currLang == 'ru'){
	$newsTitle = $model->title;
	$newsText = $model->text;
}
else{
	$newsTitle = $model->{'title_'.$currLang};
	$newsText = $model->{'text_'.$currLang};
}
if(!$newsTitle && $defLang != 'ru'){
	$newsTitle = $model->{'title_'.$defLang};
	$newsText = $model->{'text_'.$defLang};
}
elseif(!$newsTitle){
	$newsTitle = $model->title;
	$newsText = $model->text;
}

$this->title = $title?$title:$newsTitle;

?>
	<!--begin wrapper-->
	<div class="wrapper">

		<!--breadcrumb-->
		<div class="breadcrumb">
			<div class="containers">
				<a href="<?= Url::to(['/']) ?>"><?= Yii::t('easyii', 'Main') ?></a> | <a href="<?= Url::to(['/'.$module.'/']) ?>"><?= Yii::t('easyii', 'News') ?></a> | <?= $newsTitle ?>
			</div>
		</div>
		<div class="text-page">

			<div class="left-column">
				<div class="last-docs-date"><p class="p-last-docs"><?= date('d.m.Y, H:i', $model->time) ?></p></div>
				
				<b><?= $newsTitle ?></b>
				<?php if($model->image): ?>
				<img src="<?= Image::thumb($model->image, 710) ?>" alt="<?= $newsTitle ?>">
				<?php endif; ?>
				
				<?= $newsText ?>
				
				<div class="print-link">
					<a href="javascript:window.print(); void 0;"><?= Yii::t('easyii', 'Print') ?></a>
				</div>
			</div>
			<div class="right-column">

				<a href="#" class="download-button">популярне серед користувачів</a>

				<div class="all-documents-box">
					<a href="<?= Url::to(['/'.$module.'/']) ?>" class="blue-button all-documents">всі новини</a>
				</div>
				<!--end all-documents-button-->

			</div>

		</div>
		<!--text-page EnD-->
		<!--cos-link-->
			<div class="cos-link">
				<ul>
					<li class='twit'><a href="*"><span>Поділитися</span> 135</a></li>
					<li class='fb'><a href="*"><span>Поділитися</span> 135</a></li>
					<li class='vk'><a href="*"><span>Поділитися</span> 135</a></li>
					<li class='gplus'><a href="*"><span>Поділитися</span> 135</a></li>
				</ul>
			</div>
		<!--cos-link EnD-->

	</div>
	<!--end wrapper-->
