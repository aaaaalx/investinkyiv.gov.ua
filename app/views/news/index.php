<?php
use yii\helpers\Url;
use yii\easyii\modules\news\api\News;
use vendor\noumo\easyii\models\Lang;
use yii\easyii\helpers\Image;

$this->title = Yii::t('easyii', 'News');

$module = 'news';

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;

?>
	<!--begin wrapper-->
	<div class="wrapper">

		<!--breadcrumb-->
		<div class="breadcrumb">
			<div class="containers">
				<a href="<?= Url::to(['/']) ?>"><?= Yii::t('easyii', 'Main') ?></a> | <?= Yii::t('easyii', 'News') ?>
			</div>
		</div>
		<div class="news-page">
			<div class="title-page"><?= Yii::t('easyii', 'News') ?></div>
			
		<?php if(count($items)>0): ?>
		<?php foreach($items as $item): ?>
		
		    <?php
			if($currLang == 'ru'){
			    $newsTitle = $item->title;
			    $newsShort = $item->short;
			}
			else{
			    $newsTitle = $item->{'title_'.$currLang};
				$newsShort = $item->{'short_'.$currLang};
			}
			if(!$newsTitle && $defLang != 'ru'){
				$newsTitle = $item->{'title_'.$defLang};
				$newsShort = $item->{'short_'.$defLang};
			}
			elseif(!$newsTitle){
			    $newsTitle = $item->title;
			    $newsShort = $item->short;
			}		
			?>
			
		<?php if($newsTitle): ?>
			
		<div class="news-block">
		    <?php if($item->image): ?>
			<img src="<?= Image::thumb($item->image, 380, 190, true) ?>" alt="<?= $newsTitle ?>">
			<?php endif; ?>
			<div class="news-text">
				<div class="news-date"><p class="p-date"><?= date('d.m.Y, H:i', $item->time) ?></p></div>
				<a href="<?= Url::to(['/'.$module.'/'.$item->slug]) ?>" class="a-news"><?= $newsTitle ?></a>
				<?= $newsShort ?>
			</div>
		</div>
		
		<?php endif; ?>
		
		<?php endforeach; ?>
		<?php endif; ?>
        
		<div class="clearfix"></div>
		
		<?php echo \yii\widgets\LinkPager::widget(['pagination' => $pagination, 'options' => ['class' => 'pagination clearfix']]); ?>

		</div>
		<!--text-page EnD-->

	</div>
	<!--end wrapper-->
