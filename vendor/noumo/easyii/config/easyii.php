<?php

return [
    'modules' => [
        'admin' => [
            'class' => 'yii\easyii\AdminModule',
        ],
    ],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                //'admin/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => 'admin/<controller>/<action>',
                'admin/<module:\w+>/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => 'admin/<module>/<controller>/<action>'
            ],
         ],
        'user' => [
            'identityClass' => 'yii\easyii\models\Admin',
            'enableAutoLogin' => true,
            'authTimeout' => 86400,
        ],
        'i18n' => [
            'translations' => [
                'easyii' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/messages',
                    'fileMap' => [
                        'easyii' => 'admin.php',
                    ]
                ],
				'article' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/modules/article/messages',
                    'fileMap' => [
                        'article' => 'admin.php',
                    ]
                ],
				'carousel' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/modules/carousel/messages',
                    'fileMap' => [
                        'carousel' => 'admin.php',
                    ]
                ],
				'catalog' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/modules/catalog/messages',
                    'fileMap' => [
                        'catalog' => 'admin.php',
                    ]
                ],
				'faq' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/modules/faq/messages',
                    'fileMap' => [
                        'faq' => 'admin.php',
                    ]
                ],
				'feedback' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/modules/feedback/messages',
                    'fileMap' => [
                        'feedback' => 'admin.php',
                    ]
                ],
				'file' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/modules/file/messages',
                    'fileMap' => [
                        'file' => 'admin.php',
                    ]
                ],
				'gallery' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/modules/gallery/messages',
                    'fileMap' => [
                        'gallery' => 'admin.php',
                    ]
                ],
				'guestbook' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/modules/guestbook/messages',
                    'fileMap' => [
                        'guestbook' => 'admin.php',
                    ]
                ],
				'news' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/modules/news/messages',
                    'fileMap' => [
                        'news' => 'admin.php',
                    ]
                ],
				'page' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/modules/page/messages',
                    'fileMap' => [
                        'page' => 'admin.php',
                    ]
                ],
				'text' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/modules/text/messages',
                    'fileMap' => [
                        'text' => 'admin.php',
                    ]
                ],
                'shopcart' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/modules/shopcart/messages',
                    'fileMap' => [
                        'shopcart' => 'admin.php',
                    ]
                ],				
				'subscribe' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/modules/subscribe/messages',
                    'fileMap' => [
                        'subscribe' => 'admin.php',
                    ]
                ],
                'menu' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@easyii/modules/menu/messages',
                    'fileMap' => [
                        'menu' => 'admin.php',
                    ]
                ],				
            ],
        ],
        'formatter' => [
            'sizeFormatBase' => 1000
        ],
    ],
    'bootstrap' => ['admin']
];