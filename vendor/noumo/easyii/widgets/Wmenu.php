<?php
namespace yii\easyii\widgets;

use vendor\noumo\easyii\modules\menu\models\Menu;
use vendor\noumo\easyii\modules\menu\models\MenuItems;
use yii\base\InvalidConfigException;

class Wmenu extends \yii\bootstrap\Widget
{

	public $view;
	public $menu;
	public $cssClass;
	public $type;
	public $title;
	public $link;

    public function init(){}

    public function run() {
	    $menuObj = Menu::find()->where('id = :id AND status = 1', [':id' => $this->menu])->one();
		$menuRootItemsObj = MenuItems::find()->where('parent is null AND status = 1 AND menu = :menu', [':menu'=>$this->menu])->all();
		if(count($menuRootItemsObj)>0){
		    $newRootList = [];
		    foreach($menuRootItemsObj as $val){
			
			//get link
			
			//get children
			
			}
			return $this->render('menu/'.(isset($this->view)?$this->view:'main'), [
                'menu' => $menuObj,
			    'items' => $menuRootItemsObj,
				'cssClass' => isset($this->cssClass)?$this->cssClass:'menu',
				'type' => isset($this->type)?$this->type:null,
				'title' => isset($this->title)?$this->title:null,
				'link' => isset($this->link)?$this->link:null
            ]);
		}
    }
}