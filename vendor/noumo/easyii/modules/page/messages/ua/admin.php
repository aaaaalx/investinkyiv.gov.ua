<?php
return [
    'Pages' => 'Сторінки',
    'Create page' => 'Створити сторінку',
    'Edit page' => 'Редагувати сторінку',
    'Page created' => 'Сторінку створено успішно',
    'Page updated' => 'Сторінку оновлено',
    'Page deleted' => 'Сторінку видалено',
	'Category' => 'Категорія',
	'-Choose a category-' => '-Оберіть категорію-',
	'Published' => 'Опубліковано',
	'Unpublished' => 'Не опубіиковано',
	'Status' => 'Статус',
	'Translation has been added for language: ' => 'Переказ болу додано до мов: ',
	'Translation has not been added for language: ' => 'Переказ було додано до мови: ',
];