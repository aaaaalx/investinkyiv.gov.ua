<?php
return [
    'Menu' => 'Меню',
    'Create menu' => 'Створити меню',
    'Edit menu' => 'Редагувати меню',
    'Menu created' => 'Меню створено успішно',
    'Menu updated' => 'Меню оновлено',
    'Menu deleted' => 'Меню видалено',
	'Create menu item' => 'Створити пункт меню',
	'General options for all languages' => 'Основні опції для усіх мов',
	'Item type' => 'Тип пункту меню',
	'Select resource' => 'Обрати джерело меню',
	'Link' => 'Посилання',
	'Parent item' => 'Батьківський пункт меню',
	'Status' => 'Статус',
	'Name' => 'Назва',
	'-Select resource-' => '-Обрати джерело-',
	'-Select type-' => '-Обрати тип меню-',
	'-Select parent item-' => '-Обрати батьківський пункт меню-',
	'Published' => 'Опубліковано',
	'Unpublished' => 'Не опубліковано',
	'Menu item created' => 'Пункт меню створений',
	'Edit menu item' => 'Редагувати пункт меню',
	'Menu item updated' => 'Пункт меню оновлен',
	'Menu item deleted' => 'Пункт меню видален',
];