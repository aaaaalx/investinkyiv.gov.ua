<?php
return [
    'Subscribe' => 'E-mail розсилка',
	'E-mail subscribe' => 'E-mail розсилка',
    'Subscribers' => 'Підписчики',
    'Create subscribe' => 'Створити розсилку',
    'History' => 'Історія',
    'View subscribe history' => 'Перегляд історії розсилки',
    'Subscriber deleted' => 'Підписчика видалено',
    'Subscribe successfully created and sent' => 'Розсилку створено успішно та відправлено',
    'Subject' => 'Тема',
    'Body' => 'Повідомлення',
    'Sent' => 'Відправлено',
    'Unsubscribe' => 'Відмовитись від розсилки'
];