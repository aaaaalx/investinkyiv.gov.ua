<?php

return [
    'Installation' => 'Встановлення',
    'Install' => 'Встановити',
    'Root password' => 'Пароль розробника',
    'Admin E-mail' => 'E-mail алдміністратора',
    'Robot E-mail' => 'E-mail розсильника',
    'Auth time' => 'Час авторизації',
    'Frontend toolbar position' => 'ППозиція панелі на сайті',
    'Password to login as root' => 'Пароль для входу як root',
    'Used as "ReplyTo" in mail messages' => 'Адреса "ReplyTo", під час відправлення листів',
    'Used as "From" in mail messages' => 'Адреса "From", під час відправлення листів',
    'Required for using captcha in forms (guestbook, feedback)' => 'Необхідно для використання каптчі у формах (зворотній зв`язок, книга для гостей)',
    'You easily can get keys on' => 'Вы маєте можливість легко отримати ключі задля',
    'ReCaptcha website' => 'сайту ReCaptcha',
    'Installation completed' => 'Встановлення завершено',
    'Installation error' => 'Помилка встановленняи',
    'Cannot connect to database. Please configure `{0}`.' => 'Помилка під час підключення до бази даних. Будь ласка, перевірте налаштування `{0}`.',
    'EasyiiCMS is already installed. If you want to reinstall easyiiCMS, please drop all tables with prefix `easyii_` from your database manually.' => 'EasyiiCMS наразі встановлена. Якщо ві бажаєте встановити заново, видаліть усі таблиці із префіксом `easyii_` в вашей базе данных.'
];