<?php
use yii\helpers\Url;
use vendor\noumo\easyii\models\Lang;

$asset = \app\assets\AppAsset::register($this);

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;

if($currLang == 'ru'){
	$pageTitle = $model->title;
	$pageText = $model->text;
	$pageCode = $model->code;
}
else{
	$pageTitle = $model->{'title_'.$currLang};
	$pageText = $model->{'text_'.$currLang};
	$pageCode = $model->{'code_'.$currLang};
}
if(!$pageTitle && $defLang != 'ru'){
	$pageTitle = $model->{'title_'.$defLang};
	$pageText = $model->{'text_'.$defLang};
	$pageCode = $model->{'code_'.$defLang};
}
elseif(!$pageTitle){
	$pageTitle = $model->title;
	$pageText = $model->text;
	$pageCode = $model->code;
}

$this->title = $title?$title:$pageTitle;

?>
		<div class="empty-block-header clearfix"></div>
		
		<!--breadcrumb-->
		<div class="breadcrumb noprint">
			<div class="containers">
				<a href="<?= Url::to(['/']) ?>"><?= Yii::t('easyii', 'Main') ?></a> | <?= $pageTitle ?>
			</div>
		</div>
		
			<div class="text-page clearfix">
				<div class="title-page"><?= $pageTitle ?></div>
				<div class="containers clearfix">
				<br>
                <?= $pageText ?>
			    <?= $pageCode ?>
				<br>
				</div>
			</div>
		
		<div class="print-link gray-block">
			<a href="javascript:window.print(); void 0;"><?= Yii::t('easyii', 'Print') ?></a>
		</div>