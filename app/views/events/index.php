<?php
use yii\helpers\Url;
use vendor\noumo\easyii\models\Lang;
use yii\easyii\helpers\Image;

$this->title = Yii::t('easyii', 'Events');

use yii\easyii\modules\news\api\Article;

$module = 'events';

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;

?>
	<!--begin wrapper-->
	<div class="wrapper">

		<!--breadcrumb-->
		<div class="breadcrumb">
			<div class="containers">
				<a href="<?= Url::to(['/']) ?>"><?= Yii::t('easyii', 'Main') ?></a> | <?= Yii::t('easyii', 'Events') ?>
			</div>
		</div>
		<div class="news-page">
			<div class="title-page"><?= Yii::t('easyii', 'Events') ?></div>
			
		<?php if(count($items)>0): ?>
		<?php foreach($items as $item): ?>
		
		    <?php
			if($currLang == 'ru'){
			    $eventsTitle = $item->title;
			    $eventsShort = $item->short;
			}
			else{
			    $eventsTitle = $item->{'title_'.$currLang};
				$eventsShort = $item->{'short_'.$currLang};
			}
			if(!$eventsTitle && $defLang != 'ru'){
				$eventsTitle = $item->{'title_'.$defLang};
				$eventsShort = $item->{'short_'.$defLang};
			}
			elseif(!$eventsTitle){
			    $eventsTitle = $item->title;
			    $eventsShort = $item->short;
			}
			?>
			
		<?php if($eventsTitle): ?>
			
		<div class="news-block">
		    <?php if($item->image): ?>
			<img src="<?= Image::thumb($item->image, 380, 190, true) ?>" alt="<?= $eventsTitle ?>">
			<?php endif; ?>
			<div class="news-text">
				<div class="news-date"><p class="p-date"><?= date('d.m.Y, H:i', $item->time) ?></p></div>
				<a href="<?= Url::to(['/'.$module.'/'.$item->slug]) ?>" class="a-news"><?= $eventsTitle ?></a>
				<?= $eventsShort ?>
			</div>
		</div>
		
		<?php endif; ?>
		
		<?php endforeach; ?>
		<?php endif; ?>
		
		<div class="clearfix"></div>
		
		<?php 
		echo \yii\widgets\LinkPager::widget(['pagination' => $pagination, 'options' => ['class' => 'pagination clearfix']]); 
		?>

		</div>
		<!--text-page EnD-->
		<!--cos-link-->
			<div class="cos-link">
				<ul>
					<li class='twit'><a href="*"><span>Поділитися</span> 135</a></li>
					<li class='fb'><a href="*"><span>Поділитися</span> 135</a></li>
					<li class='vk'><a href="*"><span>Поділитися</span> 135</a></li>
					<li class='gplus'><a href="*"><span>Поділитися</span> 135</a></li>
				</ul>
			</div>
		<!--cos-link EnD-->

	</div>
	<!--end wrapper-->
