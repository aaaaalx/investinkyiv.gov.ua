<?php
return [
    'Texts' => 'Текстові блоки',
	'Text blocks' => 'Текстові блоки',
    'Create text' => 'Створити текст',
    'Edit text' => 'Редагувати текст',
    'Text created' => 'Текст створено успішно',
    'Text updated' => 'Текст оновлено',
    'Text deleted' => 'Текст видалено'
];