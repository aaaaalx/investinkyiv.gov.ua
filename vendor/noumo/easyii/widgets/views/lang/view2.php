<?php
use yii\helpers\Html;
?>
<div class="lang">
<div class="lang-but"></div>
<a href="<?= '/'.$current->url.Yii::$app->getRequest()->getLangUrl() ?>" class="lang-select"><img src="<?= $asset->baseUrl ?>/img/flag-<?= $current->url ?>.png" alt="<?= $current->url ?>"><?= strtoupper($current->url) ?></a>
<?php foreach ($langs as $lang):?>
<a href="<?= '/'.$lang->url.Yii::$app->getRequest()->getLangUrl() ?>" class="lang-select"><img src="<?= $asset->baseUrl ?>/img/flag-<?= $lang->url ?>.png" alt="<?= $lang->url ?>"><?= strtoupper($lang->url) ?></a>
<?php endforeach;?>
</div>
