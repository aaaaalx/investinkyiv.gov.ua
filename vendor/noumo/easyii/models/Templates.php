<?php

namespace vendor\noumo\easyii\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "easyii_templates".
 *
 * @property integer $id
 * @property string $name
 * @property string $path
 * @property string $type
 * @property integer $status
 * @property string $description
 */
class Templates extends \yii\db\ActiveRecord
{

    public static function getTemplatesList($type = null)
    {
	    $result = [];
	    if(!$type){
	        $res = self::find()->where('status = 1')->all();
		}
		else{
		    $res = self::find()->where('status = 1 AND type = :type', [':type' => $type])->all();
		}
		if(count($res)>0){
		    foreach($res as $val){
			    $result[$val->path] = Yii::t('easyii', $val->name);
			}
		}
        return $result;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'easyii_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'status'], 'required'],
            [['status'], 'integer'],
            [['name', 'path', 'type'], 'string', 'max' => 128],
            [['description'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('easyii', 'ID'),
            'name' => Yii::t('easyii', 'Name'),
            'path' => Yii::t('easyii', 'Path'),
            'type' => Yii::t('easyii', 'Type'),
            'status' => Yii::t('easyii', 'Status'),
            'description' => Yii::t('easyii', 'Description'),
        ];
    }

    /**
     * @inheritdoc
     * @return TemplatesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TemplatesQuery(get_called_class());
    }
}
