<?php
use yii\easyii\helpers\Image;
use yii\easyii\widgets\DateTimePicker;
use yii\easyii\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;

use yii\bootstrap\Tabs;
use vendor\noumo\easyii\models\Lang;
use yii\bootstrap\Collapse;

$module = $this->context->module->id;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>

<?php

$itemsList = [];
$itemsList[] = [
	'label' => 'Українська',
	'content' => langField($form, $model, 'ua', Lang::getDefaultLang()->url),
	'active' => true
];
$itemsList[] = [
	'label' => 'Русский',
	'content' => langField($form, $model, 'ru', Lang::getDefaultLang()->url),
	'active' => false
];
$itemsList[] = [
	'label' => 'English',
	'content' => langField($form, $model, 'en', Lang::getDefaultLang()->url),
	'active' => false
];

?>

<?php echo Tabs::widget([
    'items' => $itemsList
]);
?>

<?php
function langField($form, $model, $lang, $defLang){
    if($lang == 'ru'){
        $title = 'title';
	    $short = 'short';
	    $text = 'text';
	}
	else{
        $title = 'title_'.$lang;
	    $short = 'short_'.$lang;
	    $text = 'text_'.$lang;
	}
	$res = '';
	$res .= $form->field($model, $title);
	$res .= $form->field($model, $short)->textarea();
	$res .= $form->field($model, $text)->widget(Redactor::className(),[
        'options' => [
            'minHeight' => 400,
            'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'article']),
            'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'article']),
            'plugins' => ['fullscreen']
        ]
    ]);
	if(IS_ROOT){
	    $res .= SeoForm::widget(['model' => $model, 'lang' => $lang]);
	}
	return $res;
}
?>

<?php echo Tabs::widget([
    'items' => [
	    [
	        'label' => Yii::t('easyii','General options for all languages'),
		    'active' => true
		]
	],
]);
?>

<?php if($this->context->module->settings['articleThumb']) : ?>
    <?php if($model->image) : ?>
        <img src="<?= Image::thumb($model->image, 240) ?>">
        <a href="<?= Url::to(['/admin/'.$module.'/items/clear-image', 'id' => $model->primaryKey]) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear image')?>"><?= Yii::t('easyii', 'Clear image')?></a>
    <?php endif; ?>
    <?= $form->field($model, 'image')->fileInput() ?>
<?php endif; ?>

<?
echo Collapse::widget([
        'items' => [
            [
                'label' => Yii::t('easyii','Attach file'),
                'content' => addDocument($model, $module, $form),
                'contentOptions' => [
                    //'class' => 'in'
                ]
            ]
        ]
    ]);
	
function addDocument($model, $module, $form){
    $res = '';
    if($model->document){
        $res .= '<a href="'.Url::to(['/admin/'.$module.'/items/clear-document', 'id' => $model->primaryKey]).'" class="text-danger confirm-delete" title="'.Yii::t('easyii', 'Clear document').'">'.Yii::t('easyii', 'Clear document').' '.substr($model->document, strrpos($model->document, '/')+1).'</a>';
    }
	$res .= $form->field($model, 'document')->fileInput();
	return $res;
}
?>

<?= $form->field($model, 'time')->widget(DateTimePicker::className()); ?>

<?php if($this->context->module->settings['enableTags']) : ?>
    <?= $form->field($model, 'tagNames')->widget(TagsInput::className()) ?>
<?php endif; ?>

<?php if(IS_ROOT) : ?>
    <?= $form->field($model, 'slug') ?>
<?php endif; ?>

<?= $form->field($model, 'calendar')->textInput()->widget(DateTimePicker::className()); ?>

<?= $form->field($model, 'template')->dropDownList(\vendor\noumo\easyii\models\Templates::getTemplatesList('article')) ?>

<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>

<?php if(!$model->template): ?>
<?php $this->registerJs("
        jQuery(function($){
	        $('[name=\'Item[calendar]\']').val('');
	        $('#dtp-item-calendar').find($('.calendar')).val('');
        });
	");
?>
<?php endif; ?>