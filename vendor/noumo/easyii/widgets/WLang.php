<?php
namespace yii\easyii\widgets;

use vendor\noumo\easyii\models\Lang;
use yii\base\InvalidConfigException;

class WLang extends \yii\bootstrap\Widget
{

    public $view;
	public $asset;

    public function init(){}

    public function run() {
        return $this->render('lang/'.(isset($this->view)?$this->view:'view'), [
            'current' => Lang::getCurrent(),
            'langs' => Lang::find()->where('id != :current_id', [':current_id' => Lang::getCurrent()->id])->all(),
			'asset' => isset($this->asset)?$this->asset:'',
        ]);
    }
}