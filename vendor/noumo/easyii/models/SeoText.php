<?php
namespace yii\easyii\models;

use Yii;
use yii\easyii\validators\EscapeValidator;

class SeoText extends \yii\easyii\components\ActiveRecord
{
    public static function tableName()
    {
        return 'easyii_seotext';
    }

    public function rules()
    {
        return [
            [['h1', 'title', 'keywords', 'description', 'h1_en', 'title_en', 'keywords_en', 'description_en', 'h1_ua', 'title_ua', 'keywords_ua', 'description_ua'], 'trim'],
            [['h1', 'title', 'keywords', 'description', 'h1_en', 'title_en', 'keywords_en', 'description_en', 'h1_ua', 'title_ua', 'keywords_ua', 'description_ua'], 'string', 'max' => 255],
            [['h1', 'title', 'keywords', 'description', 'h1_en', 'title_en', 'keywords_en', 'description_en', 'h1_ua', 'title_ua', 'keywords_ua', 'description_ua'], EscapeValidator::className()],
        ];
    }

    public function attributeLabels()
    {
        return [
            'h1' => 'Seo H1',
            'title' => 'Seo Title',
            'keywords' => 'Seo Keywords',
            'description' => 'Seo Description',
			'h1_en' => 'Seo H1',
            'title_en' => 'Seo Title',
            'keywords_en' => 'Seo Keywords',
            'description_en' => 'Seo Description',
			'h1_ua' => 'Seo H1',
            'title_ua' => 'Seo Title',
            'keywords_ua' => 'Seo Keywords',
            'description_ua' => 'Seo Description',
        ];
    }

    public function isEmpty()
    {
        //return (!$this->h1 && !$this->title && !$this->keywords && !$this->description);
		return (!$this->h1 && !$this->title && !$this->keywords && !$this->description &&
		        !$this->h1_en && !$this->title_en && !$this->keywords_en && !$this->description_en &&
				!$this->h1_ua && !$this->title_ua && !$this->keywords_ua && !$this->description_ua);
    }
}