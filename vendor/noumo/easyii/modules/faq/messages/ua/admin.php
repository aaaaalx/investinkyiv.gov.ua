<?php
return [
    'FAQ' => 'Питання та відповіді',
    'Question' => 'Питання',
    'Answer' => 'Відповідь',
    'Create entry' => 'Створити запис',
    'Edit entry' => 'Редагувати запис',
    'Entry created' => 'Запис створено успішно',
    'Entry updated' => 'Запис оновлено',
    'Entry deleted' => 'Запис видалено'
];