<?php
return [
    'Carousel' => 'Карусель',
    'Create carousel' => 'Створити карусель',
    'Edit carousel' => 'Редагувати карусель',
    'Carousel created' => 'Карусель створено успішно',
    'Carousel updated' => 'Карусель оновлено',
    'Carousel item deleted' => 'Елемент каруселі видалено'
];