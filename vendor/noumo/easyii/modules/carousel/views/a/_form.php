<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

?>
<?php $form = ActiveForm::begin([
    'enableClientValidation' => true,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); 
?>

<?php echo Tabs::widget([
    'items' => [
	    [
	        'label' => 'Українська',
		    'active' => true
		]
	],
]);
?>

<?php if($this->context->module->settings['enableTitle']) : ?>
    <?= $form->field($model, 'title_ua')->textarea() ?>
<?php endif; ?>
<?php if($this->context->module->settings['enableText']) : ?>
    <?= $form->field($model, 'text_ua')->textarea() ?>
<?php endif; ?>

<?php echo Tabs::widget([
    'items' => [
	    [
	        'label' => 'Русский',
		    'active' => true
		]
	],
]);
?>

<?php if($this->context->module->settings['enableTitle']) : ?>
    <?= $form->field($model, 'title')->textarea() ?>
<?php endif; ?>
<?php if($this->context->module->settings['enableText']) : ?>
    <?= $form->field($model, 'text')->textarea() ?>
<?php endif; ?>

<?php echo Tabs::widget([
    'items' => [
	    [
	        'label' => 'English',
		    'active' => true
		]
	],
]);
?>

<?php if($this->context->module->settings['enableTitle']) : ?>
    <?= $form->field($model, 'title_en')->textarea() ?>
<?php endif; ?>
<?php if($this->context->module->settings['enableText']) : ?>
    <?= $form->field($model, 'text_en')->textarea() ?>
<?php endif; ?>

<?php echo Tabs::widget([
    'items' => [
	    [
	        'label' => Yii::t('easyii','General options for all languages'),
		    'active' => true
		]
	],
]);
?>

<?php if($model->image) : ?>
    <img src="<?= $model->image ?>" style="width: 848px">
<?php endif; ?>
<?= $form->field($model, 'image')->fileInput() ?>
<?= $form->field($model, 'link') ?>

<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>