<?php
namespace yii\easyii\modules\menu\controllers;

use Yii;
use yii\easyii\components\Controller;
use yii\widgets\ActiveForm;
use vendor\noumo\easyii\modules\menu\models\MenuItems;
use vendor\noumo\easyii\modules\menu\models\Menu;
use vendor\noumo\easyii\models\Lang;

use yii\easyii\behaviors\StatusController;
use yii\easyii\behaviors\SortableDateController;

class ItemsController extends Controller
{
 
 	/** @var string */
    public $moduleName = 'menu';

    /** @var string  */
    public $viewRoute = '/items';
	
	public function behaviors()
    {
        return [
		    [
                'class' => SortableDateController::className(),
                'model' => MenuItems::className(),
            ],
            [
                'class' => StatusController::className(),
                'model' => MenuItems::className()
            ]
        ];
    }
	
	public function actionGetType(){
	
	    $params = Yii::$app->request->post();
		$res = [];
	    if(isset($params['addType']) && !empty($params['addType'])){
		    if($params['addType'] == MenuItems::TYPE_PAGE){
			    $arr = \yii\easyii\modules\page\models\Page::find()->where('status = 1')->asArray()->all();
				foreach($arr as $key => $val){
		            $res[] = '<option value="'.$val['page_id'].'">'.$val['title'].'</option>';
		        }
			}
			else if($params['addType'] == MenuItems::TYPE_NEWS){
			    $arr = \yii\easyii\modules\news\models\News::find()->where('status = 1')->asArray()->all();
		        foreach($arr as $key => $val){
		            $res[] = '<option value="'.$val['news_id'].'">'.$val['title'].'</option>';
		        }
			}
			else if($params['addType'] == MenuItems::TYPE_ARTICLE){
			    $arr = \yii\easyii\modules\article\models\Item::find()->where('status = 1')->asArray()->all();
		        foreach($arr as $key => $val){
		            $res[] = '<option value="'.$val['item_id'].'">'.$val['title'].'</option>';
		        }
			}
			else if($params['addType'] == MenuItems::TYPE_CATEGORY_ARTICLE){
			    $arr = \yii\easyii\modules\article\models\Category::find()->where('status = 1')->asArray()->all();
		        foreach($arr as $key => $val){
		            $res[] = '<option value="'.$val['item_id'].'">'.$val['title'].'</option>';
		        }
			}
			else if($params['addType'] == MenuItems::TYPE_CATEGORY){
			    $arr = \yii\easyii\modules\catalog\models\Category::find()->where('status = 1')->asArray()->all();
		        foreach($arr as $key => $val){
		            $res[] = '<option value="'.$val['category_id'].'">'.$val['title'].'</option>';
		        }
			}
		}
		else{
		    $res[] = '<option value="">'.Yii::t('easyii/menu','-Select resource-').'</option>';
		}
        return implode("", $res);
	}

    public function actionIndex($id)
    {
        $class = new MenuItems;
		
		$menuModel = new Menu;
		$parentMenu = $menuModel::findOne($id);

		if($parentMenu){
		    return $this->render('index', [
                'items' => $class::find()->where('menu = :menu AND parent IS NULL', [':menu' => $id])->all(),
				'id' => $id
            ]);
		}
		else{
		    return $this->redirect(['/admin/' . $this->moduleName]);
		}
    }

    public function actionCreate($id)
    {
        if(!($Menu = Menu::findOne($id))){
            return $this->redirect(['/admin/'.$this->module->id]);
        }
		
		$params = Yii::$app->request->post();
		
        $model = new MenuItems;
		
		if($params){
		    if($params['MenuItems']['parent']){
			    $parentItem = MenuItems::find()->where('id = :parent', [':parent' => $params['MenuItems']['parent']])->asArray()->one();
			    if($parentItem){
				    $params['MenuItems']['depth'] = $parentItem['depth']+1;		
				    $parentChildrenCount = count(MenuItems::find()->where('parent = :id', [':id' => $parentItem['id']])->asArray()->all());				
				    $params['MenuItems']['position'] = $parentChildrenCount+1;
			    }
		    }
		    else{
			    $params['MenuItems']['depth'] = '0';			
			    $parentChildrenCount = count(MenuItems::find()->where('parent IS NULL')->asArray()->all());
			    $params['MenuItems']['position'] = $parentChildrenCount+1;
		    }
		}
		
        if ($model->load($params)) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else {
                $model->menu= $Menu->primaryKey;

                if ($model->save()) {
                    $this->flash('success', Yii::t('easyii/menu', 'Menu item created'));
                    return $this->redirect(['/admin/'.$this->module->id.'/items/edit', 'id' => $model->primaryKey]);
                } else {
                    $this->flash('error', Yii::t('easyii', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        }
        else {
            return $this->render('create', [
                'model' => $model,
                'id' => $Menu->id,
            ]);
        }
    }

    public function actionEdit($id)
    {
        if(!($model = MenuItems::findOne($id))){
            return $this->redirect(['/admin/'.$this->module->id]);
        }
		
		$params = Yii::$app->request->post();
		
		if($params){
		    if($params['MenuItems']['parent']){
			    $parentItem = MenuItems::find()->where('id = :parent', [':parent' => $params['MenuItems']['parent']])->asArray()->one();
			    if($parentItem){
				    $params['MenuItems']['depth'] = $parentItem['depth']+1;		
				    $parentChildrenCount = count(MenuItems::find()->where('parent = :id', [':id' => $parentItem['id']])->asArray()->all());				
				    $params['MenuItems']['position'] = $parentChildrenCount+1;
			    }
		    }
		    else{
			    $params['MenuItems']['depth'] = '0';			
			    $parentChildrenCount = count(MenuItems::find()->where('parent IS NULL')->asArray()->all());
			    $params['MenuItems']['position'] = $parentChildrenCount+1;
		    }
		}

        if ($model->load($params)) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else {

                if ($model->save()) {
                    $this->flash('success', Yii::t('easyii/menu', 'Menu item updated'));
                    return $this->redirect(['/admin/'.$this->module->id.'/items/edit', 'id' => $model->primaryKey]);
                } else {
                    $this->flash('error', Yii::t('easyii', 'Update error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        }
        else {
            return $this->render('edit', [
                'model' => $model,
				'id' => $model->menu,
            ]);
        }
    }

    public function actionDelete($id)
    {
        if(($model = MenuItems::findOne($id))){
		    $children = MenuItems::find()->where('parent = :parent', ['parent' => $model->id])->all();
			if(count($children)>0){
                $this->deleteTree($children);
			}
			if(isset($model->parent)){
			    $siblings = MenuItems::find()->where('parent = :parent', ['parent' => $model->parent])->all();
			}
			else{
			    $siblings = MenuItems::find()->where('parent IS NULL')->all();
			}
			if(count($siblings)>0){
			    foreach($siblings as $val){
				    if($val->position > $model->position){
					    $val->position -= 1;
						$val->save();
					}
				}
			}
            $model->delete();
        } else {
            $this->error = Yii::t('easyii', 'Not found');
        }
        return $this->formatResponse(Yii::t('easyii/menu', 'Menu item deleted'));
    }
	public function deleteTree($children)
    {
	    if($children){
	   	    foreach($children as $child){
				$childChildern = MenuItems::find()->where('parent = :id', [':id' => $child->id])->all();
				$child->delete();
				if(count($childChildern)>0){
					$this->deleteTree($childChildern);
				}
			}
		}
	}

    public function actionUp($id)
    {
        return $this->move($id, 'up');
    }

    public function actionDown($id)
    {
        return $this->move($id, 'down');
    }

    public function actionOn($id)
    {
        return $this->changeStatus($id, MenuItems::STATUS_ON);
    }

    public function actionOff($id)
    {
        return $this->changeStatus($id, MenuItems::STATUS_OFF);
    }
	
	/**
     * Move item up/down
     *
     * @param $id
     * @param $direction
     * @return \yii\web\Response
     * @throws \Exception
     */
    private function move($id, $direction)
    {
        $modelClass = new MenuItems;

        if(($model = $modelClass::findOne($id)))
        {
            $up = $direction == 'up';
            $orderDir = $up ? SORT_ASC : SORT_DESC;

            if($model->depth == 0){

                $swapCat = $modelClass::find()->where([$up ? '>' : '<', 'order_num', $model->order_num])->orderBy(['order_num' => $orderDir])->one();
                if($swapCat)
                {
                    $modelClass::updateAll(['order_num' => '-1'], ['order_num' => $swapCat->order_num]);
                    $modelClass::updateAll(['order_num' => $swapCat->order_num], ['order_num' => $model->order_num]);
                    $modelClass::updateAll(['order_num' => $model->order_num], ['order_num' => '-1']);
                    $model->trigger(\yii\db\ActiveRecord::EVENT_AFTER_UPDATE);
                }
            } else {
                $where = [
                    'and',
                    ['tree' => $model->tree],
                    ['depth' => $model->depth],
                    [($up ? '<' : '>'), 'lft', $model->lft]
                ];

                $swapCat = $modelClass::find()->where($where)->orderBy(['lft' => ($up ? SORT_DESC : SORT_ASC)])->one();
                if($swapCat)
                {
                    if($up) {
                        $model->insertBefore($swapCat);
                    } else {
                        $model->insertAfter($swapCat);
                    }

                    $swapCat->update();
                    $model->update();
                }
            }
        }
        else {
            $this->flash('error', Yii::t('easyii', 'Not found'));
        }
        return $this->back();
    }
	
}