<?php
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use vendor\noumo\easyii\models\Lang;

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;

?>

			<div class="last-docs-box">

				<a href="<?= Url::to(['/documents/']) ?>" class="button-last-docs"><?= Yii::t('easyii', 'Last added documents') ?></a>
				
			<?php
			$documentList = \yii\easyii\modules\article\models\Item::find()->where('status = 1 AND category_id = 4 AND document IS NULL')->sortDate()->limit(3)->all();
			$itemColor = '';
			
			?>
			
			<?php if(count($documentList)>0): ?>		
			<?php foreach($documentList as $doc): ?>
			
			<?php
			if($currLang == 'ru'){
			    $documentTitle = $doc->title;
			}
			else{
			    $documentTitle = $doc->{'title_'.$currLang};
			}
			if(!$documentTitle && $defLang != 'ru'){
				$documentTitle = $doc->{'title_'.$defLang};
			}
			elseif(!$documentTitle){
			    $documentTitle = $doc->title;
			}
			?>
			
			<?php if($documentTitle): ?>

				<!--begin last-docs-->
				
				<div class="last-docs <?= $itemColor ?>">
					<div class="last-docs-date"><p class="p-last-docs"><?= date('d.m.Y', $doc->time) ?></p></div>
					<p class="p-docs"><?= $documentTitle ?></p>
				</div>
				
				<!--end last-docs-->
				
				<?php 
			    if(!$itemColor){
			        $itemColor = 'grey';
			    }
			    else{
			        $itemColor = '';
			    }
			    ?>
				
				<?php endif; ?>
			
			    <?php endforeach; ?>
			    <?php endif; ?>
				
				<a href="<?= Url::to(['/documents/']) ?>" class=" blue-button last-documents"><?= Yii::t('easyii', 'All documents') ?></a>
			

			</div>
