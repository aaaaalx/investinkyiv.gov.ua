<?php
use yii\helpers\Html;
use yii\helpers\Url;
use vendor\noumo\easyii\models\Lang;

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;
?>
<?php if(count($items)>0): ?>
<ul class="<?= $cssClass ?>">

    <?php if(isset($title) && isset($link)): ?>
    <a href="<?= $menu->checkHttp($link) ?>"><?= $title ?></a> 
    <?php elseif(isset($title)): ?>
	<span><?= $title ?></span> 
    <?php endif; ?>

    <?php foreach($items as $val): ?>
	
	<?php
	if($currLang == 'ru'){
		$name = $val->name;
	}
	else{
		$name = $val->{'name_'.$currLang};
	}
	if(!$name && $defLang != 'ru'){
		$name = $val->{'name_'.$defLang};
	}
	elseif(!$name){
		$name = $val->name;
	}
	?>
	
	<li><a href="<?= $menu->checkHttp($val->link) ?>" title="<?= $name ?>"><?= $name ?></a></li>
	<?php endforeach; ?>		
	
</ul>
<?php endif; ?>