<?php
use yii\helpers\Url;
use vendor\noumo\easyii\models\Lang;
use yii\easyii\helpers\Image;

$this->title = Yii::t('easyii', 'Search');

$module = 'search';

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;

?>
	<!--begin wrapper-->
	<div class="wrapper">

		<!--breadcrumb-->
		<div class="breadcrumb">
			<div class="containers">
				<a href="<?= Url::to(['/']) ?>"><?= Yii::t('easyii', 'Main') ?></a> | <?= Yii::t('easyii', 'Search') ?>
			</div>
		</div>
		<div class="news-page">
			<div class="title-page"><?= Yii::t('easyii', 'Search') ?></div>
			
		<?php if(count($items)>0): ?>
		<?php foreach($items as $item): ?>
		
		    <?php		
			if($currLang == 'ru'){
			    $searchTitle = $item['title'];
			}
			else{
			    $searchTitle = $item['title_'.$currLang];
			}
			if(!$searchTitle && $defLang != 'ru'){
				$searchTitle = $item['title_'.$defLang];
			}
			elseif(!$searchTitle){
			    $searchTitle = $item['title'];
			}
			
			?>
			
		<?php if($searchTitle): ?>
			
		<div class="news-block">
			<div class="news-text">
			    <?php if($item['time']): ?>
				<div class="news-date"><p class="p-date"><?= date('d.m.Y, H:i', $item['time']) ?></p></div>
				<?php endif; ?>
				<a href="<?= Url::to([$item['template'].'/'.$item['slug']]) ?>" class="a-news"><?= $searchTitle ?></a>
			</div>
		</div>
		
		<?php endif; ?>
		
		<?php endforeach; ?>
		<?php endif; ?>
		
		<div class="clearfix"></div>
		
		<?php 
		if($pagination){
		    echo \yii\widgets\LinkPager::widget(['pagination' => $pagination, 'options' => ['class' => 'pagination clearfix']]);
		}
		?>

		</div>
		<!--text-page EnD-->


	</div>
	<!--end wrapper-->
