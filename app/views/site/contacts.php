<?php
use yii\helpers\Url;
use vendor\noumo\easyii\models\Lang;

$asset = \app\assets\AppAsset::register($this);

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;

if($currLang == 'ru'){
	$pageTitle = $model->title;
	$pageText = $model->text;
	$pageCode = $model->code;
}
else{
	$pageTitle = $model->{'title_'.$currLang};
	$pageText = $model->{'text_'.$currLang};
	$pageCode = $model->{'code_'.$currLang};
}
if(!$pageTitle && $defLang != 'ru'){
	$pageTitle = $model->{'title_'.$defLang};
	$pageText = $model->{'text_'.$defLang};
	$pageCode = $model->{'code_'.$defLang};
}
elseif(!$pageTitle){
	$pageTitle = $model->title;
	$pageText = $model->text;
	$pageCode = $model->code;
}
$cssStyle = 'text-page';
if($pageCode){
    $cssStyle = 'news-page';
}
$this->title = $pageTitle;
?>

    <!--begin wrapper-->
	<div class="wrapper">

		<!--breadcrumb-->
		<div class="breadcrumb">
			<div class="containers">
				<a href="<?= Url::to(['/']) ?>"><?= Yii::t('easyii', 'Main') ?></a> | <?= $pageTitle ?>
			</div>
		</div>
		
		<div class="<?= $cssStyle ?>">
		
			<div class="title-page"><?= $pageTitle ?></div>
			
			<div class="contacts">
			
		    <?= $pageText ?>
			<?= $pageCode ?>

			<div class="print-link">
				<a href="javascript:window.print(); void 0;"><?= Yii::t('easyii', 'Print') ?></a>
			</div>
			</div>

		</div>
		<!--text-page EnD-->
		
		<!--cos-link-->
			<div class="cos-link">
				<ul>
					<li class='twit'><a href="*"><span>Поділитися</span> 135</a></li>
					<li class='fb'><a href="*"><span>Поділитися</span> 135</a></li>
					<li class='vk'><a href="*"><span>Поділитися</span> 135</a></li>
					<li class='gplus'><a href="*"><span>Поділитися</span> 135</a></li>
				</ul>
			</div>
		<!--cos-link EnD-->

	</div>
	<!--end wrapper-->