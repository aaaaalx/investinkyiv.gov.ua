<?php

namespace vendor\noumo\easyii\modules\menu\models;

use Yii;

/**
 * This is the model class for table "easyii_menu".
 *
 * @property integer $id
 * @property string $name
 * @property string $settings
 * @property integer $status
 * @property integer $time
 */
class Menu extends \yii\easyii\components\ActiveRecord
{

    const STATUS_ON = 1;
	const STATUS_OFF = 0;
	
	public function behaviors()
    {
        return [
		    'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
                ],
            ],
        ];
    }
	
	//check if url inner(short) or outer(full url)
    public static function checkHttp($url){
        if(!preg_match("~^(?:f|ht)tps?://~i", $url) && !preg_match("/^#{1}/", $url)){
		    $url = \Yii::$app->urlManager->createAbsoluteUrl($url);
		}
        return $url;
    }	

    public static function getStatusList()
    {
        return [
		    self::STATUS_ON => Yii::t('easyii/menu', 'Published'),
			self::STATUS_OFF => Yii::t('easyii/menu', 'Unpublished'),		
		];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'easyii_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['status', 'time'], 'integer'],
            [['name', 'settings'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('easyii/menu', 'ID'),
            'name' => Yii::t('easyii/menu', 'Name'),
            'settings' => Yii::t('easyii/menu', 'Settings'),
            'status' => Yii::t('easyii/menu', 'Status'),
			'time' => Yii::t('easyii/menu', 'Time'),
        ];
    }

    /**
     * @inheritdoc
     * @return MenuQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MenuQuery(get_called_class());
    }
}
