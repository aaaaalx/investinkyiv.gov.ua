<?php
namespace app\controllers;

use yii;
use yii\easyii\modules\gallery\models\Category;
use yii\easyii\models\Photo;
use yii\web\Controller;
use yii\data\Pagination;

class GalleryController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex($id)
    {
		$model = new Photo;
		$items = $model->find()->where('item_id = :id', [':id' => $id])->sort();
		$pagination = new Pagination(['totalCount' => $items->count(), 'pageSize'=>9]);
		$items = $items->offset($pagination->offset)->limit($pagination->limit)->all();
		if($items){
            return $this->render('gallery', [
		        'model' => $model,
			    'items' => $items,
			    'pagination' => $pagination
		    ]);
		}
		else{
		    throw new \yii\web\NotFoundHttpException();
		}
    }
	
	public function actionVote()
    {
        if(Yii::$app->request->isAjax){	    
			$params = Yii::$app->request->post();
			$id = $params['id'];
			$model = Photo::findOne($id);
			if($model && $params){
			    $vote = (int)$model->vote;
				$rating = (int)$model->rating;
				$value = (int)$model->value;			
				$vote += 1;
				$value += (int)$params['vote'];
				$model->value = $value;
				$model->vote = $vote;
				$rating = ceil($value/$vote);
				$model->rating = $rating;
				if($model->save()){
				    return $model->rating;
				}
			}
        }
    }
}