<?php
namespace yii\easyii\widgets;

use yii\easyii\modules\carousel\models\Carousel;
use yii\base\InvalidConfigException;

class Wslider extends \yii\bootstrap\Widget
{

    public function init(){}

    public function run() {
	    $items = Carousel::find()->where('status = 1')->all();
		if(count($items)>0){
            
			return $this->render('slider/main', [
			    'items' => $items,
            ]);
		}
    }
}