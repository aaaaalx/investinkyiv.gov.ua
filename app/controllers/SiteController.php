<?php
namespace app\controllers;

use yii\easyii\modules\page\models\Page;
use vendor\noumo\easyii\models\Lang;
use yii\web\Controller;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
	public function actionDownload($doc)
    {
	    $doc = base64_decode($doc);
		if($doc){
            return \Yii::$app->response->sendFile(\Yii::getAlias('@webroot').'/'.$doc);
		}
		else{
		    throw new \yii\web\NotFoundHttpException();
		}
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
	
	public function actionPage($slug)
    {
	    $model = new Page;
		$pageModel = $model->find()->where('slug = :slug AND status = 1', [':slug' => $slug])->one();
			
		if($pageModel){
		    $view = $pageModel->template?'//'.$pageModel->template:'//site/page';
		    //seo
            $currLang = Lang::getCurrent()->url;
		    if($currLang == 'ru'){
		        $h1 = $pageModel->getSeoText()->h1;
		        $title = $pageModel->getSeoText()->title;
		        $description = $pageModel->getSeoText()->description;
		        $keywords = $pageModel->getSeoText()->keywords;
		    }
		    else{
	    	    $h1 = $pageModel->getSeoText()->{'h1_'.$currLang};
	    	    $title = $pageModel->getSeoText()->{'title_'.$currLang};
	    	    $description = $pageModel->getSeoText()->{'description_'.$currLang};
	    	    $keywords = $pageModel->getSeoText()->{'keywords_'.$currLang};
	    	}		
	    	\Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => $description,
            ]);
		    \Yii::$app->view->registerMetaTag([
                'name' => 'keywords',
                'content' => $keywords,
            ]);
		
            return $this->render($view, [
		        'model' => $pageModel,
		    	'h1' => $h1,
		    	'title' => $title
		    ]);
		}
		else{
		    throw new \yii\web\NotFoundHttpException();
		}
    }
}