<?php
namespace yii\easyii\modules\menu\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\easyii\components\Controller;
use vendor\noumo\easyii\modules\menu\models\Menu;
use vendor\noumo\easyii\models\Lang;
use yii\widgets\ActiveForm;
use yii\easyii\behaviors\StatusController;
use yii\easyii\behaviors\SortableDateController;

class AController extends Controller
{

    public $moduleName = 'menu';

    public function behaviors()
    {
        return [
		    [
                'class' => SortableDateController::className(),
                'model' => Menu::className(),
            ],
            [
                'class' => StatusController::className(),
                'model' => Menu::className()
            ]
        ];
    }

    public function actionIndex()
    {
	    $model = new Menu;
	    $data = new ActiveDataProvider([
            'query' => Menu::find()->desc()
        ]);
        return $this->render('index', [
            'data' => $data,
			'model' => $model
        ]);
    }

    public function actionCreate()
    {
	    $model = new Menu;
	    $params = Yii::$app->request->post();
		
        if($params && $model->load($params)){
		    if(Yii::$app->request->isAjax){
				Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return ActiveForm::validate($model);
			}
            else{
                if($model->save()){
					$this->flash('success', Yii::t('easyii/menu', 'Menu created'));
				    return $this->redirect(['/admin/'.$this->module->id]);
                }
				else{
				    $this->flash('error', Yii::t('easyii', 'Create error. {0}', $model->formatErrors()).' <br>');
                    return $this->refresh();
				}
			}
		}
		else if($params){
		    $this->flash('error', Yii::t('easyii', 'Create error. {0}', $model->formatErrors()).' <br>');
            return $this->refresh();
		}
	    
        return $this->render('create', [
                'model' => $model
            ]);
    }

    public function actionEdit($id)
    {
        $class = new Menu;

        if(!($model = $class::findOne($id))){
            return $this->redirect(['/admin/' . $this->moduleName]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if($model->save()){
                    $this->flash('success', Yii::t('easyii/menu', 'Menu updated'));
                }
                else{
                    $this->flash('error', Yii::t('easyii/menu', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        }
        else {
            return $this->render('edit', [
                'model' => $model
            ]);
        }
    }

        public function actionDelete($id)
    {
        if(($model = Menu::findOne($id))){
            $model->delete();
        } else {
            $this->error = Yii::t('easyii', 'Not found');
        }
        return $this->formatResponse(Yii::t('easyii/menu', 'Menu deleted'));
    }

    public function actionUp($id)
    {
        return $this->move($id, 'up');
    }

    public function actionDown($id)
    {
        return $this->move($id, 'down');
    }

    public function actionOn($id)
    {
        return $this->changeStatus($id, Menu::STATUS_ON);
    }

    public function actionOff($id)
    {
        return $this->changeStatus($id, Menu::STATUS_OFF);
    }
}