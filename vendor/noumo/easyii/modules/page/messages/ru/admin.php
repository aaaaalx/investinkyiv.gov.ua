<?php
return [
    'Pages' => 'Страницы',
    'Create page' => 'Создать страницу',
    'Edit page' => 'Редактировать страницу',
    'Page created' => 'Страница успешно создана',
    'Page updated' => 'Страница обновлена',
    'Page deleted' => 'Страница удалена',
	'Category' => 'Категороия',
	'-Choose a category-' => '-Выберите категорию-',
	'Published' => 'Опубликовано',
	'Unpublished' => 'Не опубликовано',
	'Status' => 'Статус',
	'Translation has been added for language: ' => 'Перевод был добавлен для языка: ',
	'Translation has not been added for language: ' => 'Перевод не был добавлен для языка: ',
];