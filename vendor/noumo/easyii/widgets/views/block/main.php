<?php
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php if(!$type): ?>
<ul class="<?= $cssClass ?>">
    <?php if(count($items)>0): ?>
    <?php foreach($items as $val): ?>
	<li><a href="<?= $menu->checkHttp($val->link) ?>" title="<?= $val->name ?>"><?= $val->name ?></a></li>
	<?php endforeach; ?>		
	<?php endif; ?>
</ul>
<?php elseif($type == 'pop-up'): ?>
<ul class="<?= $cssClass ?>">
    <li class="pop-up-block"><?= Yii::t('easyii', 'Menu') ?></li>
    <?php if(count($items)>0): ?>
    <?php foreach($items as $val): ?>
	<li><a href="<?= $menu->checkHttp($val->link) ?>" title="<?= $val->name ?>"><?= $val->name ?></a></li>
	<?php endforeach; ?>	
	<?php endif; ?>
</ul>
<?php endif; ?>