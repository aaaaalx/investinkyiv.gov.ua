<?php
$this->title = Yii::t('easyii/menu', 'Edit menu item');
?>
<?= $this->render('_menu', ['parent' => $id]) ?>
<?= $this->render('_form', ['model' => $model, 'id' => $id]) ?>