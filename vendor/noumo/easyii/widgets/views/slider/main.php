<?php
use yii\helpers\Html;
use yii\easyii\widgets\Wmenu;
use vendor\noumo\easyii\models\Lang;

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;

?>
<?php if($items): ?>

        <!--begin slider-->
		<div class="slider">
			<div class="slider-container">
			
			<?php foreach($items as $val): ?>
			
				<div class="item"><img src="<?= $val->image ?>" alt="<?= $val->title ?>"></div>
				
			<?php endforeach; ?>
			
			</div>
			<div class="containers">
			<div class="slide-text">
				<div class="slide-all-text">
				
				<?php foreach($items as $val): ?>
				
				<?php
				if($currLang == 'ru'){
				    $title = $val->title;
				    $text = $val->text;
			    }
			    else{
				    $title = $val->{'title_'.$currLang};
				    $text = $val->{'text_'.$currLang};
			    }
			    if(!$title && $defLang != 'ru'){
				    $title = $val->{'title_'.$defLang};
				    $text = $val->{'text_'.$defLang};
			    }
			    elseif(!$title){
				    $title = $val->title;
				    $text = $val->text;
			    }
			    ?>
				
					<div class="slide-text-block">
						<?= $title ?>
					</div>
				
				<?php endforeach; ?>

				</div>
				<div class="arrow-prev"></div>
				<div class="arrow-next"></div>
				</div>

				<div class="invest-project">
				    <div class="title-invest-project">
					    <?= Yii::t('easyii', 'Investment projects') ?>
				    </div>
	
	                <?= Wmenu::widget(['view' => 'slider', 'menu' => 2]) ?>
				
				    <a class="all-links" href="/gotutsya-do-konkursu"><?= Yii::t('easyii', 'All investment projects') ?></a>
				
			    </div>

			</div>
			</div>
			<div class="paginator">
				<div class="pagin">
					
				</div>
			</div>
		</div>
		<!--end slider-->

<?php endif; ?>