<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;
use vendor\noumo\easyii\models\Lang;
use yii\bootstrap\Tabs;

$this->title = Yii::t('easyii/menu', 'Create menu');

?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>
