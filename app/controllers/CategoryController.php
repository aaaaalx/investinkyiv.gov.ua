<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class CategoryController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
	    echo 'Category Index';
		echo '<br>params = ';
	    $params=$_GET;
	    print_r($params);
		
	    $id_slug = Yii::$app->request->get('slug')?Yii::$app->request->get('slug'):Yii::$app->request->get('id');
        return $this->render('index', ['id_slug' => $id_slug]);
    }
	
	public function actionCategory()
    {
	
	//throw new \yii\web\HttpException(404, 'The requested Item could not be found ha ha.');
	
	echo 'Category Category';
	echo '<br>params = ';
	$params=$_GET;
	print_r($params);
	
	$Cats = Yii::$app->request->get('category');
	$CatsArr = [];
	if($Cats){
	    $CatsArr = explode('/',$Cats);
		unset($CatsArr[count($CatsArr)-1]);
	}
	echo '<br>CatsArr = ';
	print_r($CatsArr);
	
    }
	
	public function actionArticle()
    {
	echo 'Category Article';
	echo '<br>params = ';
	$params=$_GET;
	print_r($params);
	
	$Cats = Yii::$app->request->get('category');
	$article = Yii::$app->request->get('slug');
	$CatsArr = [];
	if($Cats){
	    $CatsArr = explode('/',$Cats);
	}
	echo '<br>CatsArr = ';
	print_r($CatsArr);
	echo '<br>article = ';
	print_r($article);
    }
}