<?php
return [
    'Guestbook' => 'Гостьова книга',
    'Guestbook updated' => 'Гостьову книгу оновлено',
    'View post' => 'Перегляд запису',
    'Entry deleted' => 'Запис видалено',
    'Answer' => 'Відповідь',
    'No answer' => 'Без відповіді',
    'Mark all as viewed' => 'Відмітити усі як прочитані',
    'Answer successfully saved' => 'Відповідь успішно збережено',
    'Mark as new' => 'Відмітит як нове',
    'Notify user about answer' => 'Надіслати відповідь користувачу про відповідь'
];