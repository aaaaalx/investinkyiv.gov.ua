<?php
namespace yii\easyii\modules\page\models;

use Yii;
use yii\easyii\behaviors\SluggableBehavior;
use yii\easyii\behaviors\SeoBehavior;

class Page extends \yii\easyii\components\ActiveRecord
{

    const STATUS_ON = 1;
	const STATUS_OFF = 0;

    public static function getStatusList()
    {
        return [
		    self::STATUS_ON => Yii::t('easyii/page', 'Published'),
			self::STATUS_OFF => Yii::t('easyii/page', 'Unpublished'),		
		];
    }

    public static function tableName()
    {
        return 'easyii_pages';
    }

    public function rules()
    {
        return [
            ['title_ua', 'required'],
            [['title', 'text', 'title_en', 'text_en', 'title_ua', 'text_ua', 'code', 'code_en', 'code_ua'], 'trim'],
			[['category_id', 'status', 'time'], 'integer'],
            [['title', 'title_en', 'title_ua', 'template'], 'string', 'max' => 128],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('easyii', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['slug', 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => Yii::t('easyii', 'Title'),
            'text' => Yii::t('easyii', 'Text'),
			'code' => Yii::t('easyii', 'Code'),
            'slug' => Yii::t('easyii', 'Slug'),
			'category_id' => Yii::t('easyii', 'Сategory ID'),
			'status' => Yii::t('easyii', 'Status'),
			'title_en' => Yii::t('easyii', 'Title'),
			'text_en' => Yii::t('easyii', 'Text'),
			'code_en' => Yii::t('easyii', 'Code'),
            'title_ua' => Yii::t('easyii', 'Title'),
			'text_ua' => Yii::t('easyii', 'Text'),
			'code_ua' => Yii::t('easyii', 'Code'),
			'template' => Yii::t('easyii', 'Template'),
			'time' => Yii::t('easyii', 'Time'),
        ];
    }

    public function behaviors()
    {
        return [
            'seoBehavior' => SeoBehavior::className(),
			'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => \vendor\noumo\easyii\models\Lang::getCurrentTitle(),
				'slugAttribute' => 'slug',
                'transliterator' => 'Russian-Latin/BGN; NFKD',
                'forceUpdate' => false,
                'ensureUnique' => true
            ],
			'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
                ],
            ],
        ];
    }
}