<?php
$this->title = Yii::t('easyii/menu', 'Edit menu');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>