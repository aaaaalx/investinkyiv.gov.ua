<?php

namespace vendor\noumo\easyii\modules\menu\models;

/**
 * This is the ActiveQuery class for [[MenuItems]].
 *
 * @see MenuItems
 */
class MenuItemsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MenuItems[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MenuItems|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}