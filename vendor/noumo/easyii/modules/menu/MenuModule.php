<?php
namespace yii\easyii\modules\menu;

use Yii;

class MenuModule extends \yii\easyii\components\Module
{
    public static $installConfig = [
        'title' => [
            'en' => 'Menu',
            'ru' => 'Меню',
			'ua' => 'Меню',
        ],
        'icon' => 'list-alt',
        'order_num' => 50,
    ];
}