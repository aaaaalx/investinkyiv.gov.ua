<?php
use yii\helpers\Url;
use yii\easyii\widgets\Wslider;
use vendor\noumo\easyii\models\Lang;
use yii\easyii\helpers\Image;

$asset = \app\assets\AppAsset::register($this);

$this->title = Yii::t('easyii', 'Main page');

$defLang = Lang::getDefaultLang()->url;
$currLang = Lang::getCurrent()->url;
?>

<?= Wslider::widget() ?>

			<div class="empty-block clearfix"></div>
		<div class="contant-block arrow-white arrow-top-block">
			<div class="containers clearfix">
				<div class="cont-block-title contest-title"><?= Yii::t('easyii', 'Attention! announced a contest!') ?></div>
				<div class="ad-contest">
					<div class="ad-contest-block">
						<a href="#" class="mobile touch"></a>
						<a href=""><img src="<?= $asset->baseUrl ?>/img/photo/photo.jpg" alt="">
						Lorem ipsum dolor sit amet, consectetur adipisicing dolor sit amet </a>
						<div class="ad-contest-text">
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim minim veniam, exercit...
ation ullamco laboris nisi ut aliquip ex ea commodo 
							</p>
						</div>	
					</div>
					<div class="ad-contest-block">
						<a href="#" class="mobile touch"></a>
						<a href=""><img src="<?= $asset->baseUrl ?>/img/photo/photo1.jpg" alt="">
							Lorem ipsum dolor sit amet, consectetur adipisicing dolor sit amet  </a>
						<div class="ad-contest-text">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim minim veniam, exercit...
ation ullamco laboris nisi ut aliquip ex ea commodo 
							</p>
						</div>	
					</div>
					<div class="ad-contest-block">
						<a href="#" class="mobile touch"></a>
						<a href=""><img src="<?= $asset->baseUrl ?>/img/photo/photo2.jpg" alt="">
							Lorem ipsum dolor sit amet, consectetur adipisicing dolor sit amet  </a>
						<div class="ad-contest-text">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim minim veniam, exercit...
ation ullamco laboris nisi ut aliquip ex ea commodo 
							</p>
						</div>	
						
					</div><a class="all-links" href="#"><?= Yii::t('easyii', 'All active contests') ?></a>
				</div>
			</div>
		</div>
		
		<div class="contant-block gray-block arrow-gray">
			<div class="containers clearfix">
				<div class="cont-block-title"><?= Yii::t('easyii', 'Preparing for contest') ?></div>
					<div class="ad-contest">
						<div class="contest-block">
						<a href="#" class="mobile touch"></a>
							<img src="<?= $asset->baseUrl ?>/img/photo/m-photo.jpg" alt="">
							<div class="contest-date">12.03.2015</div>
							<a href="#" class="contest-link">Lorem ipsum dolor sit amet, consectetur adipisicing ipsum dolor</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nos... </p>
						</div>
						<div class="contest-block">
						<a href="#" class="mobile touch"></a>
							<img src="<?= $asset->baseUrl ?>/img/photo/m-photo.jpg" alt="">
							<div class="contest-date">12.03.2015</div>
							<a class="contest-link" href="#">Lorem ipsum dolor sit amet, consectetur adipisicing ipsum dolor</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nos... </p>
						</div>
						<div class="contest-block">
						<a href="#" class="mobile touch"></a>
							<img src="<?= $asset->baseUrl ?>/img/photo/m-photo1.jpg" alt="">
							<div class="contest-date">12.03.2015</div>
							<a class="contest-link" href="#">Lorem ipsum dolor sit amet, consectetur adipisicing ipsum dolor</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nos... </p>
						</div>
						<div class="contest-block">
						<a href="#" class="mobile touch"></a>
							<img src="<?= $asset->baseUrl ?>/img/photo/m-photo1.jpg" alt="">
							<div class="contest-date">12.03.2015</div>
							<a class="contest-link" href="#">Lorem ipsum dolor sit amet, consectetur adipisicing ipsum dolor</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nos... </p>
						</div>
						<div class="contest-block">
						<a href="#" class="mobile touch"></a>
							<img src="<?= $asset->baseUrl ?>/img/photo/m-photo2.jpg" alt="">
							<div class="contest-date">12.03.2015</div>
							<a  class="contest-link" href="#">Lorem ipsum dolor sit amet, consectetur adipisicing ipsum dolor</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nos... </p>
						</div>
						<div class="contest-block">
						<a href="#" class="mobile touch"></a>
							<img src="<?= $asset->baseUrl ?>/img/photo/m-photo2.jpg" alt="">
							<div class="contest-date">12.03.2015</div>
							<a class="contest-link"  href="#">Lorem ipsum dolor sit amet, consectetur adipisicing ipsum dolor</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nos... </p>
						</div>
						<a class="all-links" href="#"><?= Yii::t('easyii', 'All investment projects') ?></a>
					</div>
			</div>
		</div>
		
		<div class="map-link">
			
			<div class="map-block">
				<div class="map-title"><?= Yii::t('easyii', 'INTERACTIVE MAP OF PROJECTS') ?></div>

				<a class="all-links" href="#"><?= Yii::t('easyii', 'All investment projects') ?></a>
			</div>
		</div>
			
		<div class="news-blocks arrow-white">
				<div class="containers clearfix">
			<div class="info-block">
				<div class="news-title"><?= Yii::t('easyii', 'City News') ?></div>
				<div class="info">
						<a href="#" class="mobile touch"></a>
					<div class="block-12 blue">
						12 <br>
						<span>BEP</span>
					</div>
						<a class="info-link" href="#">Lorem ipsum dolor sit consectetur adipisicing ipsum dolor</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim d minim veniam, quis nos... exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu </p>
				</div>
				<div class="info">
						<a href="#" class="mobile touch"></a>
					<div class="block-12 blue">
						12 <br>
						<span>BEP</span>
					</div>
						<a class="info-link" href="#">Lorem ipsum dolor sit consectetur adipisicing ipsum dolor</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim d minim veniam, quis nos... exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu </p>
				</div>
				<div class="info">
						<a href="#" class="mobile touch"></a>
					<div class="block-12 blue">
						12 <br>
						<span>BEP</span>
					</div>
						<a class="info-link" href="#">Lorem ipsum dolor sit consectetur adipisicing ipsum dolor</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim d minim veniam, quis nos... exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu </p>
				</div>
					
					<a class="all-links" href="#"><?= Yii::t('easyii', 'View all news') ?></a>
			</div>
			<div class="info-block">
				<div class="news-title"><?= Yii::t('easyii', 'PRESS RELEASES') ?></div>
				<div class="info">
						<a href="#" class="mobile touch"></a>
					<div class="block-12 green">
						12 <br>
						<span>BEP</span>
					</div>
					<a class="info-link" href="#">Lorem ipsum dolor sit consectetur adipisicing ipsum dolor</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim d minim veniam, quis nos... exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu </p>
				</div>
				<div class="info">
						<a href="#" class="mobile touch"></a>
					<div class="block-12 green">
						12 <br>
						<span>BEP</span>
					</div>
					<a class="info-link" href="#">Lorem ipsum dolor sit consectetur adipisicing ipsum dolor</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim d minim veniam, quis nos... exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu </p>
				</div>
				<div class="info">
						<a href="#" class="mobile touch"></a>
					<div class="block-12 green">
						12 <br>
						<span>BEP</span>
					</div>
					<a class="info-link" href="#">Lorem ipsum dolor sit consectetur adipisicing ipsum dolor</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim d minim veniam, quis nos... exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu </p>
				</div>
				
					<a class="all-links" href="#"><?= Yii::t('easyii', 'View all press releases') ?></a>
			</div>
		</div>	
		</div>
		
		<div class="success-story gray-block ">
				<div class="containers clearfix">
					<div class="story-title">
						<?= Yii::t('easyii', 'Stories of success') ?>
					</div>
					<div class="carusel">
						<div class="all-carusel">
							<div class="carusel-block">
								<div class="carusel-item">
							<img src="<?= $asset->baseUrl ?>/img/photo/photo.jpg" alt="">
							<p>Lorem ipsum dolor sit amet,  sit amet, consectetur adipisicing elit cons</p>
									
								</div>	
								<div class="carusel-item">
							<img src="<?= $asset->baseUrl ?>/img/photo/photo1.jpg" alt="">
							<p>Lorem ipsum dolor sit amet,  sit amet, consectetur adipisicing elit cons</p>
									
								</div>	
								<div class="carusel-item">
							<img src="<?= $asset->baseUrl ?>/img/photo/photo2.jpg" alt="">
							<p>Lorem ipsum dolor sit amet,  sit amet, consectetur adipisicing elit cons</p>
								
								</div>	
							</div>
							<div class="carusel-block">
								<div class="carusel-item">
							<img src="<?= $asset->baseUrl ?>/img/photo/photo.jpg" alt="">
							<p>Lorem ipsum dolor sit amet,  sit amet, consectetur adipisicing elit cons</p>
									
								</div>	
								<div class="carusel-item">
							<img src="<?= $asset->baseUrl ?>/img/photo/photo1.jpg" alt="">
							<p>Lorem ipsum dolor sit amet,  sit amet, consectetur adipisicing elit cons</p>
									
								</div>	
								<div class="carusel-item">
							<img src="<?= $asset->baseUrl ?>/img/photo/photo2.jpg" alt="">
							<p>Lorem ipsum dolor sit amet,  sit amet, consectetur adipisicing elit cons</p>
								
								</div>	
							</div>
						</div>
					</div>
					<div class="info tablet">

						<a href="#">Lorem ipsum dolor sit consectetur adipisicing ipsum dolor</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim d minim veniam, quis nos... exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu </p>
				</div>
				<div class="info tablet">

						<a href="#">Lorem ipsum dolor sit consectetur adipisicing ipsum dolor</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim d minim veniam, quis nos... exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu </p>
				</div>
				<div class="info tablet">

						<a href="#">Lorem ipsum dolor sit consectetur adipisicing ipsum dolor</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim d minim veniam, quis nos... exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu </p>
				</div>

						<div class="carusel-arrow-prev"></div>
						<div class="carusel-arrow-next"></div>
						
						<a class="all-links" href="#"><?= Yii::t('easyii', 'View all press releases') ?></a>
			</div>
			
		</div>