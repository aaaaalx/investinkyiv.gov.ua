<?php
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\Html;

BootstrapPluginAsset::register($this);

$labelOptions = ['class' => 'control-label'];
$inputOptions = ['class' => 'form-control'];

if('ru' == $lang){
    $lang = '';
}
else{
    $lang = '_'.$lang;
}

?>
<p>
    <a class="dashed-link collapsed" data-toggle="collapse" href="#seo-form<?= $lang ?>" aria-expanded="false" aria-controls="seo-form<?= $lang ?>"><?= Yii::t('easyii', 'Seo texts')?></a>
</p>

<div class="collapse" id="seo-form<?= $lang ?>">
    <div class="form-group">
        <?= Html::activeLabel($model, 'h1'.$lang, $labelOptions) ?>
        <?= Html::activeTextInput($model, 'h1'.$lang, $inputOptions) ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'title'.$lang, $labelOptions) ?>
        <?= Html::activeTextInput($model, 'title'.$lang, $inputOptions) ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'keywords'.$lang, $labelOptions) ?>
        <?= Html::activeTextInput($model, 'keywords'.$lang, $inputOptions) ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'description'.$lang, $labelOptions) ?>
        <?= Html::activeTextarea($model, 'description'.$lang, $inputOptions) ?>
    </div>
</div>