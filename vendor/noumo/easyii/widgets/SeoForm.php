<?php
namespace yii\easyii\widgets;

use Yii;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use vendor\noumo\easyii\models\Lang;

class SeoForm extends Widget
{
    public $model;
	public $lang;

    public function init()
    {
        parent::init();

        if (empty($this->model)) {
            throw new InvalidConfigException('Required `model` param isn\'t set.');
        }
    }

    public function run()
    {
        echo $this->render('seo_form', [
            'model' => $this->model->seoText,
			'lang' => $this->lang,
			'langDefault' => Lang::getDefaultLang()->url
        ]);
    }

}