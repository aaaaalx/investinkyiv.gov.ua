<?php
return [
    'Articles' => 'Статті',
    'Article' => 'Стаття',

    'Short' => 'Короткий текст',

    'Create article' => 'Створити статтю',
    'Edit article' => 'Редагувати статтю',
    'Article created' => 'Нову статтю створено успішно',
    'Article updated' => 'Статтю оновлено',
    'Article deleted' => 'Статтю видалено',
];