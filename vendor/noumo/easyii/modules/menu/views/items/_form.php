<?php
use yii\easyii\helpers\Image;
use yii\easyii\widgets\DateTimePicker;
use yii\easyii\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;
use yii\bootstrap\Tabs;
use vendor\noumo\easyii\models\Lang;
use yii\bootstrap\Collapse;

$className = get_class($model);
$modelClass = substr($className, strrpos($className, '\\')+1);
?>

<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['class' => 'model-form']
]); ?>

<?php

$itemsList = [];
$itemsList[] = [
	'label' => 'Українська',
	'content' => langField($form, $model, 'ua', Lang::getDefaultLang()->url),
	'active' => true
];
$itemsList[] = [
	'label' => 'Русский',
	'content' => langField($form, $model, 'ru', Lang::getDefaultLang()->url),
	'active' => false
];
$itemsList[] = [
	'label' => 'English',
	'content' => langField($form, $model, 'en', Lang::getDefaultLang()->url),
	'active' => false
];

?>

<?php echo Tabs::widget([
    'items' => $itemsList
]);
?>

<?php
function langField($form, $model, $lang, $defLang){
    if($lang == 'ru'){
        $name = 'name';
	}
	else{
        $name = 'name_'.$lang;
	}
	$res = '';
	$res .= $form->field($model, $name);
	return $res;
}
?>

<?php echo Tabs::widget([
    'items' => [
	    [
	        'label' => Yii::t('easyii','General options for all languages'),
		    'active' => true
		]
	],
]);
?>

<?= $form->field($model, 'menu')->hiddenInput(['value' => $id])->label(false) ?>

<?= ''/*$form->field($model, 'type')->dropDownList($model::getTypesList(), ['prompt' => Yii::t('easyii/menu','-Select type-')])->label(Yii::t('easyii/menu','Item type'))*/ ?>

<?= ''/*$form->field($model, 'link_id')->dropDownList([], ['prompt' => Yii::t('easyii/menu','-Select resource-')])->label(Yii::t('easyii/menu','Select resource'))*/ ?>

<?= $form->field($model, 'link') ?>

<?
echo Collapse::widget([
        'items' => [
            [
                'label' => Yii::t('easyii','Settings'),
                'content' => addDocument($model, $form),
                'contentOptions' => [
                    //'class' => 'in'
                ]
            ]
        ]
    ]);
	
function addDocument($model, $form){
	return $form->field($model, 'settings')->textarea();
}
?>

<?= $form->field($model, 'parent')->dropDownList($model::getParent($id, $model->id), ['prompt' => Yii::t('easyii/menu','-Select parent item-')])->label(Yii::t('easyii/menu','Parent item')) ?>

<?= $form->field($model, 'status')->dropDownList($model::getStatusList())->label(Yii::t('easyii/menu','Status')) ?>

<?= Html::submitButton(Yii::t('easyii','Save'), ['name' => 'subButt', 'value' => $currentLang['name'], 'id' => 'subButt', 'class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>

	<?php $this->registerJs("
    jQuery(function($){
        $('#menuitems-type').on('change', function(){
		    $.ajax({
                url: '".Url::to(['@web/admin/menu/items/get-type/'], true)."',
                type: 'post',
                data: {addType: $('#menuitems-type').val()},
                success: function (data) {
					$('#menuitems-link_id').html(data);
                }
            });
        });
    });");?>